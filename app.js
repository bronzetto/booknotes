require("dotenv").config();

const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");

const usersRouter = require("./routes/users");
const myBooksRouter = require("./routes/myBooks");
const bestBooksRouter = require("./routes/bestBooks");
const wishBooksRouter = require("./routes/wishBooks");
const dislikedBooksRouter = require("./routes/dislikedBooks");
const commentsRouter = require("./routes/comments");
const commentedBooksRouter = require("./routes/commentedBooks");
const booksRouter = require("./routes/books");

const passport = require("passport");
const authenticate = require("./authenticate");

const config = require("./config");
const env = process.env.NODE_ENV || "development";
const mongoose = require("mongoose");

mongoose.set("useCreateIndex", true);

const database =
  config[env] === undefined ? process.env.MONGODB_URI : config[env].mongoUrl;

const connect = mongoose.connect(database, {
  useNewUrlParser: true,
  //useUnifiedTopology: true,
});

connect.then(
  (db) => {
    console.log("Connected correctly to server", db.connections[0].name);
  },
  (err) => {
    console.log(err);
  }
);

const app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(passport.initialize());

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, "public/build")));

app.use("/users", usersRouter);
app.use("/api/bestBooks", bestBooksRouter);
app.use("/api/wishBooks", wishBooksRouter);
app.use("/api/dislikedBooks", dislikedBooksRouter);
app.use("/api/commentedBooks", commentedBooksRouter);
app.use("/api/comments", commentsRouter);
app.use("/api/myBooks", myBooksRouter);
app.use("/api/books", booksRouter);

app.get("/*", (req, res, next) => {
  res.redirect(`/?${req.params[0]}`);
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
  // res.redirect("/");
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
