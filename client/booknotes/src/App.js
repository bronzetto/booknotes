import React, { Component } from "react";
import { createBrowserHistory } from "history";
import MainContainer from "./components/mainContainer/MainContainer";
import RouterWithHistory from "./RouterWithHistory";

import "./App.css";

export const history = createBrowserHistory();

class App extends Component {
  render() {
    return (
      <RouterWithHistory history={history}>
        <div className="App container">
          <MainContainer />
        </div>
      </RouterWithHistory>
    );
  }
}

export default App;
