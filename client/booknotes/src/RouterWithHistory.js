import React from "react";

import { Router } from "react-router-dom";

const RouterWithHistory = props => {
  return <Router history={props.history}>{props.children}</Router>;
};

export default RouterWithHistory;
