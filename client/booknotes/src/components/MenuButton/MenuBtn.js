import React, { Component } from "react";
import { Button } from "reactstrap";
import Styles from "./MenuBtn.module.css";

import { withRouter } from "react-router-dom";

const Icon = props => {
  return props.type === "my-books" ? (
    <span className={Styles.Icon + " fas fa-book fa-lg"} />
  ) : props.type === "wish-books" ? (
    <span className={Styles.Icon + " fas fa-grin-hearts fa-lg"} />
  ) : props.type === "disliked-books" ? (
    <span className={Styles.Icon + " fas fa-book-dead fa-lg"} />
  ) : props.type === "commented" ? (
    <span className={Styles.Icon + " fas fa-comments fa-lg"} />
  ) : props.type === "on-movies" ? (
    <span className={Styles.Icon + " fas fa-video fa-lg"} />
  ) : props.type === "on-amazon" ? (
    <span className={Styles.Icon + " fab fa-amazon fa-lg"} />
  ) : props.type === "search" ? (
    <span className={Styles.Icon + " fas fa-search fa-xs"} />
  ) : (
    ""
  );
};

class MenuBtn extends Component {
  buttonClassesSearch(isSearchActive) {
    // console.log(isSearchActive, "??");

    const buttonBasicStylesSearch =
      Styles.Button +
      " w-100 my-2 py-2 d-flex flex-row justify-content-between align-items-center mt-2 search";
    return isSearchActive
      ? Styles.SearchBtnSelected + " " + buttonBasicStylesSearch
      : buttonBasicStylesSearch + " " + Styles.SearchBtn;
  }

  buttonClasses(currentPage, type) {
    const buttonBasicStyles =
      Styles.Button +
      " w-100 my-1 py-3 d-flex justify-content-center justify-content-lg-between align-items-center sm-pr-0 lg-pr-5";
    return currentPage && currentPage.indexOf(type) > -1
      ? Styles.ButtonSelected + " " + buttonBasicStyles
      : buttonBasicStyles;
  }

  render() {
    const {
      currentPage,
      type,
      changePage,
      isSearchActive,
      children
    } = this.props;

    // console.log(this.props, "<<<<<<<");
    if (type === "search") {
      return (
        <Button
          size="sm"
          color="info"
          className={this.buttonClassesSearch(isSearchActive)}
          onClick={() => changePage(type)}
        >
          <p className={Styles.ParagraphSearch + " m-0 mr-2"}>
            {children.toUpperCase()}
          </p>
          <Icon {...this.props} />
        </Button>
      );
    } else {
      return (
        <Button
          size="sm"
          color="primary"
          className={this.buttonClasses(currentPage, type)}
          onClick={() => changePage(type)}
        >
          <p className={Styles.Paragraph + " lead m-0 ml-3 d-none d-lg-block"}>
            {children.toUpperCase()}
          </p>

          <div className="d-sm-flex justify-content-center align-item-center d-md-block">
            <Icon {...this.props} />
          </div>
        </Button>
      );
    }
  }
}

export default withRouter(MenuBtn);
