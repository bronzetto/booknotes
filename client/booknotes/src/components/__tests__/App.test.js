import React from "react";
import "../setupTests";
import { shallow } from "enzyme";
import App from "../../App";
import MainContainer from "../mainContainer/MainContainer";

it("render a div with App container classes", () => {
  const wrapped = shallow(<App />);
  expect(wrapped.find(MainContainer).length).toEqual(1);
});
