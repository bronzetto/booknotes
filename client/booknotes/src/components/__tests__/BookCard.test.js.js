import React, { Component } from "react";
import "../setupTests";
import { mount, shallow } from "enzyme";
import BookCard, { Card } from "../bookCard/BookCard";
import MainContainer from "../mainContainer/MainContainer";
const book = {
  authors: ["bob"],
  categories: ["cazzarasa"],
  userOwner: ["..."],
  _id: "5ceaea1460e24432482363cc",
  googleId: "oeQ0HIuQw5YC",
  title: "Antonio Vivaldi",
  image:
    "http://books.google.com/books/content?id=oeQ0HIuQw5YC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api",
  createdAt: "2019-05-26T19:33:40.572Z",
  updatedAt: "2019-05-26T19:33:40.572Z"
};
describe("<BookCard />", () => {
  it("render a BookCard component", () => {
    // function trackImageOnload() {
    //   Object.defineProperty(Image.prototype, "onload", {
    //     get: function() {
    //       return this._onload;
    //     },
    //     set: function(fn) {
    //       imageOnload = fn;
    //       this._onload = fn;
    //     }
    //   });
    // }
    // trackImageOnload(() => false);
    const wrapper = shallow(<BookCard book={book} />);
    expect(wrapper.find(".bookCard").length).toEqual(1);
    expect(wrapper.find(".img-thumbnail").length).toEqual(1);
    console.log((Image.prototype.onLoad = false), wrapper.debug());
    //wrapper.instance().setIsImageLoading(false);
    //wrapper.setState({ isImageLoading: false });
    //setIsImageLoading(false);
    expect(wrapper.find("Card").length).toEqual(1);
  });

  // it("doesn't render a spinner if isActive = false", () => {
  //   const wrapper = mount(<Spinner isActive={false} />);
  //   expect(wrapper.find("span").length).toEqual(0);
  // });
});
