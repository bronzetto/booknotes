import React, { Component } from "react";
import "../setupTests";
import { mount, shallow } from "enzyme";
import BooksGroup, { DisplayGroup, NoBooks } from "../booksGroup/BooksGroup";

describe("<BooksGroup />", () => {
  const books = [
    { volumeInfo: { title: "A" } },
    { volumeInfo: { title: "A" } }
  ];

  it("renders a list of books if there are books", () => {
    const wrapped = shallow(<BooksGroup books={books} />);
    expect(wrapped.find(DisplayGroup).length).toEqual(1);
  });
  it("renders a a text if no books are passed", () => {
    const wrapped = shallow(<BooksGroup books={[]} />);
    expect(wrapped.find(NoBooks).length).toEqual(1);
  });
});
