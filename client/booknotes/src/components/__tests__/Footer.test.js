import React, { Component } from "react";
import "../setupTests";
import { mount, shallow } from "enzyme";
import Footer from "../footer/Footer";
import { Navbar } from "reactstrap";
import { Link } from "react-router-dom";

describe("<Footer />", () => {
  let wrapped;
  beforeEach(() => {
    wrapped = shallow(<Footer />);
  });
  it("renders a NavBar component", () => {
    expect(wrapped.find(Navbar).length).toEqual(1);
  });

  it("renders a Link component", () => {
    expect(wrapped.find(Link).length).toEqual(1);
  });

  it("renders a div component with class socials", () => {
    expect(wrapped.find(".socials").length).toEqual(1);
  });
});
