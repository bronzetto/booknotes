import React from "react";
import "../setupTests";
import { mount, shallow } from "enzyme";
import ReduxProvider from "../../ReduxProvider";
import HomeViewContainer from "../homeViewContainer/HomeViewContainer";
import { Button } from "reactstrap";
import RouterWithHistory from "../../RouterWithHistory";
import { createBrowserHistory } from "history";
export const history = createBrowserHistory();

describe("<HomeViewContainer />", () => {
  let wrapped;
  beforeEach(() => {
    wrapped = mount(
      <RouterWithHistory history={history}>
        <ReduxProvider>
          <HomeViewContainer />
        </ReduxProvider>
      </RouterWithHistory>
    );
    // console.log(wrapped.debug());
  });
  afterEach(() => {
    wrapped.unmount();
  });
  it("renders a Title a Lead and a image component", () => {
    expect(wrapped.find("h1.Title").length).toEqual(1);
    expect(wrapped.find("h2.Lead").length).toEqual(1);
    expect(wrapped.find(".Image").length).toEqual(1);
    expect(wrapped.find("Button").length).toEqual(2);
  });
});
