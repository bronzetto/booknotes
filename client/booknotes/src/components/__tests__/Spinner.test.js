import React, { Component } from "react";
import "../setupTests";
import { mount, shallow } from "enzyme";
import Spinner from "../spinner/Spinner";
import MainContainer from "../mainContainer/MainContainer";

describe("<Spinner />", () => {
  it("render a spinner if isActive = true", () => {
    const wrapped = shallow(<Spinner isActive={true} />);
    expect(wrapped.find("span").length).toEqual(1);
  });

  it("doesn't render a spinner if isActive = false", () => {
    const wrapped = mount(<Spinner isActive={false} />);
    expect(wrapped.find("span").length).toEqual(0);
  });
});
