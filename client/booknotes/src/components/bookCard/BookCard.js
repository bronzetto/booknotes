import React, { useState, useEffect } from "react";
import { Media, Button, Alert } from "reactstrap";
import Spinner from "../spinner/Spinner";
import Styles from "./BookCard.module.css";

export const Card = ({ book, deleteCurrentBook }) => {
  return (
    <div className="body h-100 position-relative mb-0">
      <p className={`${Styles.Title}`}> {book.title}</p>
      <Button
        className={`position-absolute b-0 ${Styles.Button}`}
        size="sm"
        color="warning"
        onClick={() => deleteCurrentBook(book)}
      >
        Delete Book
      </Button>
    </div>
  );
};
const BookCard = props => {
  const [placeHolderImage, setPlaceHolderImage] = useState(
    "https://via.placeholder.com/128x158"
  );
  const [animateAwayClass, setAnimateAwayClass] = useState("");

  return (
    <Media
      className={`my-3 h-100 bookCard ${Styles.MediaCard} ${animateAwayClass}`}
    >
      <Media left href="#">
        <Media
          className={`img-thumbnail ${Styles.Image} ${"visible"}`}
          object
          src={props.book.image}
          alt={props.book.title}
        />
        <div className="m-0 p-0">
          <Spinner isActive={false} />
        </div>
      </Media>
      <Media className="h-75" body>
        <Media className="h-100 mb-0" heading>
          <Card book={props.book} deleteCurrentBook={props.deleteCurrentBook} />
        </Media>
      </Media>
    </Media>
  );
};

export default BookCard;
