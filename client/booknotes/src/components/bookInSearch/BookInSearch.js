import { useState, useEffect } from "react";
import { Media, Button, Alert } from "reactstrap";
import Spinner from "../spinner/Spinner";
import ErrorBoundary from "../errorBoundary/ErrorBoundary";
import { readableCurrentPage, getExerpt } from "../../config/utilFunctions";
import Styles from "./BookInSearch.module.css";

function BookInSearch({ isSaved, currentPage, book, handleSaveBook }) {
  const [activeSpinner, setActiveSpinner] = useState(false);

  function getPage() {
    return readableCurrentPage(currentPage).toLowerCase();
  }

  function saveBook() {
    setActiveSpinner(true);
    setTimeout(() => {
      handleSaveBook(book);
    }, 100);
  }

  const { title, description, authors, imageLinks } = book.volumeInfo;
  const placeholderImage = "https://via.placeholder.com/128x158";

  return (
    <ErrorBoundary place="Book in search">
      <div className=" col-12 col-md-12 col-lg-6">
        <Media className={`${Styles.Media}`}>
          <Media className="pr-2 mb-sm-3" left>
            <Media
              object
              src={imageLinks ? imageLinks.smallThumbnail : placeholderImage}
              alt={title}
            />
          </Media>
          <Media body>
            <Media heading>
              {title ? getExerpt(title, 40) : "No title is available"}
            </Media>

            <div className="mediaBody">
              <p className="description">
                {description
                  ? getExerpt(description, 100)
                  : "No description available"}
              </p>
              <p className="ont-weight-bold my-2">
                AUTHORS:{" "}
                {authors ? authors.join(", ") : "No Authors are available"}
              </p>
            </div>

            {!isSaved ? (
              <Button
                className="my-md-2"
                color="primary"
                size="sm"
                onClick={() => saveBook()}
              >
                Add to {getPage()}
                <Spinner isActive={activeSpinner} />
              </Button>
            ) : (
              <Alert style={{ padding: "3px 10px" }} color="success">
                Saved in {getPage()}
              </Alert>
            )}
          </Media>
        </Media>
        <hr />
      </div>
    </ErrorBoundary>
  );
}

export default BookInSearch;
