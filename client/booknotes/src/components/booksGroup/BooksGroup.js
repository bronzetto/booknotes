import React, { Component } from "react";
import BookCard from "../bookCard/BookCard";
import { CSSTransition, TransitionGroup } from "react-transition-group";

export const NoBooks = props => {
  return (
    <div className="col-md-12">
      <h3 className="noFoundText mt-5 bg-info text-white h-100 pt-5 text-center">
        Sorry, There are no books in your list
      </h3>
    </div>
  );
};

export const DisplayGroup = props => {
  const { books } = props;
  return (
    <TransitionGroup component={null}>
      {books.map(book => {
        return (
          <CSSTransition
            key={book._id || Math.random() * 2000 + 0}
            timeout={500}
            classNames="fade"
            component={null}
          >
            <div className="col-md-6" key={book._id}>
              <BookCard
                book={book}
                deleteCurrentBook={props.deleteCurrentBook}
              />
            </div>
          </CSSTransition>
        );
      })}
    </TransitionGroup>
  );
};

export default function BooksGroup(props) {
  const { books } = props;
  return books && books.length > 0 ? (
    <DisplayGroup
      books={books}
      deleteCurrentBook={props.deleteCurrentBook}
      {...props}
    />
  ) : (
    <NoBooks />
  );
}
