import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  fetchSavedBooksFromDb,
  setCurrentPage,
  deleteCurrentBook
} from "../../config/actionCreators";
import { readableCurrentPage } from "../../config/utilFunctions";
import Styles from "./BooksViewContainer.module.css";
import MenuBtn from "../MenuButton/MenuBtn";
import BooksGroup from "../booksGroup/BooksGroup";

class BooksViewContainer extends Component {
  componentDidMount() {
    if (this.props.PageState.currentPage !== this.props.path) {
      this.props.setCurrentPage(this.props.path);
      this.props.fetchSavedBooksFromDb(this.props.path);
    }

    this.scrollViewDeayed(0);
  }

  scrollViewDeayed(delay) {
    const booksView = document.querySelector(".booksView");
    setTimeout(() => {
      booksView.scroll({ top: 0, behavior: "smooth" });
    }, delay);
  }

  componentDidUpdate(prevProps) {
    const bookField = this.getBookField(this.props.path);
    if (this.props[bookField].books !== prevProps[bookField].books) {
      this.scrollViewDeayed(0);
    }
  }

  getBookField() {
    return readableCurrentPage(this.props.path)
      .split(" ")
      .join("");
  }

  deleteCurrentBook(book) {
    const bookField = this.props.path.split("/")[1];

    // console.log("%%%%%%", this.props.path, bookField, book);
    this.props.deleteCurrentBook(bookField, book);
  }

  render() {
    const { isSearchActive, changePage } = this.props;
    const bookField = this.getBookField(this.props.path);

    return (
      <div className={`container`}>
        <div className="header border-bottom d-flex flex-row justify-content-between px-2">
          <div className="title">
            <h1 className={`text-center my-2 ${Styles.FieldTitle}`}>
              {readableCurrentPage(this.props.path).toUpperCase()}
            </h1>
          </div>
          <div className="searchButton pt-1">
            <MenuBtn
              type="search"
              isSearchActive={isSearchActive}
              changePage={changePage}
            >
              Add Books
            </MenuBtn>
          </div>
        </div>
        <div className={`content ${Styles.Content} booksView`}>
          {/* List of this page books */}
          <div className="row">
            <BooksGroup
              books={this.props[bookField].books}
              deleteCurrentBook={this.deleteCurrentBook.bind(this)}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => state;

const mapDispatchToProps = state => dispatch => {
  return {
    setCurrentPage: page => dispatch(setCurrentPage(page)),
    deleteCurrentBook: (bookField, book) =>
      dispatch(deleteCurrentBook(bookField, book)),
    fetchSavedBooksFromDb: bookField =>
      dispatch(fetchSavedBooksFromDb(bookField))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(BooksViewContainer)
);
