import React from "react";
import { Link } from "react-router-dom";

const CreditsViewContainer = () => {
  return (
    <div className="credits-container container">
      <h1>
        We want to credit here all the resources we used to create this app:
      </h1>
      <ul>
        <li>Google books api</li>
        <li>Amazon api</li>
        <li>www.flaticon.com</li>
      </ul>

      <Link to="/my-books">
        back to my books span.
        <i className="fa fa-arrow-right" aria-hidden="true" />{" "}
      </Link>
    </div>
  );
};

export default CreditsViewContainer;
