import React, { Component } from "react";
import PropTypes from "prop-types";
import { Alert } from "reactstrap";

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      place: "",
      error: null,
      errorInfo: null
    };
  }

  componentDidCatch(error, info) {
    this.setState(state => ({
      ...state,
      error: error,
      place: this.props.place,
      errorInfo: info
    }));
  }

  render() {
    if (this.state.error) {
      return (
        <div className="p-5">
          <Alert color="danger">
            <p>
              Sorry, something wasn't quite right in {this.state.place}! <br />
              {this.state.error && this.state.error.toString()}
              <br />
              <br />
              <br />
              {this.state.errorInfo.componentStack}
            </p>
          </Alert>
        </div>
      );
    } else {
      return this.props.children;
    }
  }
}

ErrorBoundary.propTypes = {
  place: PropTypes.string.isRequired
};

export default ErrorBoundary;
