import React from "react";
import { Alert } from "reactstrap";
import Styles from "./ErrorMessage.module.css";

const ErrorMessage = ({ errorMess }) => {
  return (
    <Alert
      className={!errorMess ? Styles.HideAlert : Styles.ShowAlert}
      color="danger"
    >
      {errorMess}
    </Alert>
  );
};
export default ErrorMessage;
