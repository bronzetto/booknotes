import React from "react";
import { Navbar } from "reactstrap";

import { Link } from "react-router-dom";

import {
  FacebookShareButton,
  FacebookIcon,
  GooglePlusShareButton,
  GooglePlusIcon
} from "react-share";

import Styles from "./Footer.module.css";

const centerAlign =
  "d-flex flex-row justify-content-space-between align-items-center";
const Footer = () => (
  // <div className={Styles.FooterContainer}>
  <Navbar
    className={`${Styles.footer} ${Styles.FooterContainer} fixed-bottom d-flex`}
    color="dark"
    dark
  >
    <Link to="/credits">...credit</Link>
    <div className={`${centerAlign} socials h-100 align-self-baseline`}>
      <FacebookShareButton url={"https://www.google.com"}>
        <FacebookIcon className="d-flex" size={35} round={true} />
      </FacebookShareButton>

      <div className="mx-2" />

      <GooglePlusShareButton url={"https://www.google.com"}>
        <GooglePlusIcon className="d-flex" size={35} round={true} />
      </GooglePlusShareButton>
    </div>
  </Navbar>
  // </div>
);

export default Footer;
