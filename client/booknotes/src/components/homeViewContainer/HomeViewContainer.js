import React, { Component } from "react";
import { Jumbotron, Button } from "reactstrap";
import { setCurrentPage } from "../../config/actionCreators";

import { connect } from "react-redux";

import { NavLink } from "react-router-dom";

import Styles from "./HomeViewContainer.module.css";

class HomeViewContainer extends Component {
  componentDidMount() {
    this.props.setCurrentPage("home");
  }

  render() {
    const { User } = this.props;

    return (
      <Jumbotron className={Styles.Jb + " h-75"}>
        <h1 className={Styles.Title + " display-3 mt-4"}>
          Welcome to books shelfy!
        </h1>
        <h2 className={Styles.Lead + " lead mb-5"}>
          A simple place that helps you keep your library organized.
        </h2>
        {!User.id ? (
          <p className="lead">
            <NavLink to="/login">
              <Button className="my-1" color="primary" size="lg">
                Login Now
              </Button>
            </NavLink>
            <span className="mx-3">or</span>
            <NavLink to="/register">
              <Button className="my-1" color="primary" size="lg">
                Register Now
              </Button>
            </NavLink>
          </p>
        ) : (
          <NavLink to="/my-books">
            <h4 className="start-link">Start to organise your library!</h4>
          </NavLink>
        )}

        <div className={Styles.Image + " d-none d-md-block"}>
          <img
            src="assets/images/piled-books.png"
            className="img-fluid"
            alt="Books pile"
          />
        </div>
      </Jumbotron>
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => {
  return {
    setCurrentPage: page => dispatch(setCurrentPage(page))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeViewContainer);
