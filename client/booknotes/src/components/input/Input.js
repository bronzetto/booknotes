import React, { Component } from "react";
import { FormGroup, Label } from "reactstrap";
import { Control, Errors } from "react-redux-form";
import {
  required,
  maxLength,
  minLength,
  validEmail,
  validPassword
} from "../../config/utilFunctions";

export default class Input extends Component {
  render() {
    const { inputType, passToCompare, setPass, setRepeatPass } = this.props;
    const model = "." + inputType;
    const notDefaultInput = ["password", "repeatPassword", "email"];
    const capitalize = word => {
      if (word)
        return (
          word.split("")[0].toUpperCase() +
          word
            .split("")
            .splice(1, word.length - 1)
            .join("")
        );
    };

    if (notDefaultInput.indexOf(inputType) === -1) {
      return (
        <FormGroup>
          <Label htmlFor={inputType}>
            {capitalize(inputType)
              .split("_")
              .join(" ")}
          </Label>
          <Control.text
            model={model}
            type="text"
            id={inputType}
            name={inputType}
            className="form-control"
            validators={{
              required,
              minLength: minLength(3),
              maxLength: maxLength(15)
            }}
          />
          <Errors
            className="text-danger"
            model={model}
            show="touched"
            messages={{
              required: "Required -",
              minLength: " Must be greater than 2 characters ",
              maxLength: " Must be 15 characters or less "
            }}
          />
        </FormGroup>
      );
    } else if (inputType === "email") {
      return (
        <FormGroup>
          <Label htmlFor="email">Email</Label>
          <Control.text
            model=".email"
            type="email"
            id="email"
            name="email"
            className="form-control"
            validators={{
              required,
              validEmail
            }}
          />
          <Errors
            className="text-danger"
            model=".email"
            show="touched"
            messages={{
              required: "Required -",
              validEmail: " Invalid Email Address "
            }}
          />
        </FormGroup>
      );
    } else if (inputType === "password") {
      return (
        <FormGroup>
          <Label htmlFor="password">Password</Label>
          <Control.text
            model=".password"
            type="password"
            id="password"
            name="password"
            className="form-control"
            onBlur={e => setPass(e)}
            validators={{
              required,
              minLength: minLength(6)
            }}
          />
          <Errors
            className="text-danger"
            model=".password"
            show="touched"
            messages={{
              required: "Required",
              minLength: " Must be greater than 6 characters "
            }}
          />
        </FormGroup>
      );
    } else if (inputType === "repeatPassword") {
      return (
        <FormGroup>
          <Label htmlFor="repeatPassword">Repeat Password</Label>
          <Control.text
            model=".repeatPassword"
            type="password"
            id="repeatPassword"
            name="repeatPassword"
            className="form-control"
            onBlur={e => setRepeatPass(e)}
            validators={{
              required,
              minLength: minLength(6),
              validPassword: validPassword(passToCompare)
            }}
          />
          <Errors
            className="text-danger"
            model=".repeatPassword"
            show="touched"
            messages={{
              required: "Required -",
              minLength: " Must be greater than 6 characters ",
              validPassword: " Repeat password must be same as Password "
            }}
          />
        </FormGroup>
      );
    } else {
      return "";
    }
  }
}
