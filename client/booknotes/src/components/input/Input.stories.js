import React from "react";

import { storiesOf } from "@storybook/react";
import { action, configureActions } from "@storybook/addon-actions";
import Input from "./Input";

import { Provider } from "react-redux";
import store from "../../../config/store";

// import Styles from "./MainNav.module.css";

// export const actions = {
//   LogUserIn: action("LogUserIn"),
//   LogUserOut: action("LogUserOut")
// };

storiesOf("Inputs", module)
  .addDecorator(getStory => <Provider store={store}>{getStory()}</Provider>)
  .addWithJSX("Input username", () => <Input inputType="username" />)
  .addWithJSX("Input email", () => <Input inputType="email" />);
// .addWithJSX("Input password", () => <Input inputType="password" />);
