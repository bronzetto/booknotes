import React, { Component } from "react";
import { Button } from "reactstrap";
import { LocalForm } from "react-redux-form";
import { connect } from "react-redux";
import { history } from "../../App";
import { NavLink } from "react-router-dom";
import {
  loginUser,
  setCurrentPage,
  loginError,
} from "../../config/actionCreators";
import Input from "../input/Input";
import Spinner from "../spinner/Spinner";
import ErrorMessage from "../errorMessage/ErrorMessage";
import SuccessMessage from "../successMessage/SuccessMessage";

import { Redirect } from "react-router-dom";

import Styles from "./LoginViewContainer.module.css";

class LoginViewContainer extends Component {
  componentDidMount = () => {
    this.props.setCurrentPage("login");

    // if (!this.props.User.id) this.props.loginError(null);

    if (this.props.User.token) history.push("/my-books");
  };

  handleLogin = (values) => {
    this.props.loginUser(values);
  };

  render() {
    const { User } = this.props;
    return !User.userId ? (
      <div className={Styles.FormContainer + " my-5 container"}>
        <h1 className="my-4">Login Here</h1>
        <LocalForm onSubmit={this.handleLogin}>
          <Input inputType="email" />

          <Input inputType="password" setPass={() => null} />

          <ErrorMessage {...User} />
          <SuccessMessage {...User} />

          <Button
            className="my-3"
            type="submit"
            value="submit"
            color="primary"
            size="lg"
          >
            Login Now
            <Spinner isActive={User.isLoading} />
          </Button>
        </LocalForm>
        <hr />
        <div className="not-register">
          <p>
            Don't have an account yet?{" "}
            <NavLink to="/register">Register here</NavLink>
          </p>
        </div>

        <div className="forgot-password">
          <p>
            Forgot your password?{" "}
            <NavLink to="/reset-password">Reset password here</NavLink>
          </p>
        </div>
        <p>
          If you don't want to use your email you can test with: <br />
          Email: test@email.com <br /> Password: password <br />
        </p>
      </div>
    ) : (
      <Redirect to="/my-books" />
    );
  }
}

const mapStateToProps = (state) => state;
const mapDispatchToProps = (dispatch) => {
  return {
    loginUser: (values) => dispatch(loginUser(values)),
    loginError: (values) => dispatch(loginError(values)),
    setCurrentPage: (page) => dispatch(setCurrentPage(page)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginViewContainer);
