import React, { Component } from "react";

import ProtectedRoute from "../protectedRoute/ProtectedRoute";
import MainNav from "../mainNav/MainNav";
import Footer from "../footer/Footer";
import HomeViewContainer from "../homeViewContainer/HomeViewContainer";
import LoginViewContainer from "../loginViewContainer/LoginViewContainer";
import RegisterViewContainer from "../registerViewContainer/RegisterViewContainer";
import ResetPassViewContainer from "../resetPassViewContainer/ResetPassViewContainer";
import ReplacePassViewContainer from "../replacePassViewContainer/ReplacePassViewContainer";
import CreditsViewContainer from "../creditsViewContainer/CreditsViewContainer";
import BooksViewContainer from "../booksViewContainer/BooksViewContainer";
import NotFound from "../notFound/NotFound";

import ErrorBoundary from "../errorBoundary/ErrorBoundary";

import Styles from "./MainContainer.module.css";

import { connect } from "react-redux";
import { checkIfLogged, logoutUser } from "../../config/actionCreators";

import { Switch, Route, withRouter, Redirect } from "react-router-dom";

export class MainContainer extends Component {
  state = {
    fadePageIn: true,
  };
  componentDidMount() {
    this.props.checkIfLogged();
  }

  isLoggedUser() {
    return this.props.User.id ? true : false;
  }

  render() {
    return (
      <ErrorBoundary place="root">
        <div className={`${Styles.MainContainer}`}>
          <MainNav user={this.props.User} LogUserOut={this.props.logUserOut} />

          <div
            className={`d-none ${
              this.state.fadePageIn ? "animated fadeIn d-block" : ""
            }`}
          >
            <Switch>
              <Route exact path="/" component={HomeViewContainer} />

              <Route path="/login" component={LoginViewContainer} />

              <Route path="/register" component={RegisterViewContainer} />

              <Route path="/credits" component={CreditsViewContainer} />

              <Route
                path="/reset-password"
                component={ResetPassViewContainer}
              />

              <Route
                path="/replace-password/:token"
                component={ReplacePassViewContainer}
              />

              <ProtectedRoute
                path="/my-books"
                loggedIn={this.isLoggedUser()}
                Component={BooksViewContainer}
              />

              <ProtectedRoute
                path="/wish-books"
                loggedIn={this.isLoggedUser()}
                Component={BooksViewContainer}
              />

              <ProtectedRoute
                path="/disliked-books"
                loggedIn={this.isLoggedUser()}
                Component={BooksViewContainer}
              />

              <ProtectedRoute
                path="/commented"
                loggedIn={this.isLoggedUser()}
                Component={BooksViewContainer}
              />

              <ProtectedRoute
                path="/on-movies"
                loggedIn={this.isLoggedUser()}
                Component={BooksViewContainer}
              />

              <ProtectedRoute
                path="/on-amazon"
                loggedIn={this.isLoggedUser()}
                Component={BooksViewContainer}
              />

              <Route exact path="/*" component={() => <NotFound />} />

              {/* <Redirect from="/*" to="/404" /> */}
            </Switch>
          </div>
          <Footer />
        </div>
      </ErrorBoundary>
    );
  }
}

// How about a game were user needs to guess a title book from description? with categories and scores

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (state) => (dispatch) => {
  return {
    checkIfLogged: () => dispatch(checkIfLogged()),
    logUserOut: () => dispatch(logoutUser()),
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(MainContainer)
);
