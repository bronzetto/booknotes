import React, { Component } from "react";
import Styles from "./MainNav.module.css";
import { Navbar, Nav, NavItem } from "reactstrap";

import { NavLink } from "react-router-dom";
import { Dropdown, DropdownToggle, DropdownMenu } from "reactstrap";

class MainNav extends Component {
  state = {
    dropdownOpen: false,
    userName: null
  };

  toggle = () => {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  };

  handleLogout = () => {
    this.toggle();
    this.props.LogUserOut("Log User Out");
  };

  render() {
    const { LogUserIn, user } = this.props;
    return (
      <div>
        <Navbar className="py-3" color="dark" dark>
          <NavLink className="d-flex flex-direction-row" to="/">
            <h1 className={Styles.Brand}>Bookshelfy</h1>
            {/* <small className="d-flex align-items-end mx-3">pre-alpha</small> */}
          </NavLink>
          <Nav className="ml-auto flex-row" navbar>
            {user.firstName ? (
              <NavItem className="d-flex flex-row">
                <Dropdown
                  className={Styles.Dropdown}
                  isOpen={this.state.dropdownOpen}
                  toggle={this.toggle}
                >
                  <DropdownToggle
                    tag="a"
                    className={"nav-link " + Styles.Dropdown}
                    caret
                    onClick={() => this.toggle}
                    // onMouseEnter={event => {
                    //   if (
                    //     event.target.getAttribute("aria-expanded") === "false"
                    //   )
                    //     return this.toggle;
                    // }}
                  >
                    {user.firstName}
                  </DropdownToggle>
                  <DropdownMenu
                    className={Styles.DropdownContent}
                    onMouseLeave={this.toggle}
                    right
                  >
                    {/* <p className={Styles.Log + " mx-3 " + Styles.Pointer}>
                      Manage Account
                    </p>

                    <DropdownItem divider /> */}

                    <p
                      className={
                        Styles.Log + " mx-3 " + Styles.Pointer + " logout"
                      }
                      onClick={() => this.handleLogout()}
                    >
                      Logout
                      {/* <span
                          className={Styles.User + " ml-2 fa fa-sign-out fa-lg"}
                        /> */}
                    </p>
                  </DropdownMenu>
                </Dropdown>
              </NavItem>
            ) : (
              <NavItem>
                <NavLink to="/login" className={Styles.Link}>
                  <p
                    className={
                      Styles.Log + " " + Styles.Pointer + " m-0 ml-5 login"
                    }
                    onClick={() => LogUserIn}
                  >
                    Login
                    <span
                      className={Styles.User + " ml-2 fa fa-sign-in fa-lg"}
                    />
                  </p>
                </NavLink>
              </NavItem>
            )}
          </Nav>
        </Navbar>
      </div>
    );
  }
}

export default MainNav;
