import React from "react";

import { storiesOf } from "@storybook/react";
import { action, configureActions } from "@storybook/addon-actions";
import MainNav from "./MainNav";

import Styles from "./MainNav.module.css";

export const actions = {
  LogUserIn: action("LogUserIn"),
  LogUserOut: action("LogUserOut")
};

storiesOf("Main Navigation", module)
  .addWithJSX("User Logged In", () => (
    <MainNav {...actions} userName="Roberto" />
  ))
  .addWithJSX("User Logged Out", () => <MainNav {...actions} userName="" />);
