import React from "react";
import { Link } from "react-router-dom";
import MenuBtn from "../MenuButton/MenuBtn";

import Styles from "./Menu.module.css";

const Menu = props => {
  return (
    <div className={`${Styles.Basic} border-left`}>
      <div className="row pt-1">
        <div className={Styles.NoPad + " col-4 col-lg-12 my-2"}>
          {/* <Link to="/my-books"> */}
          <MenuBtn
            type="my-books"
            currentPage={props.currentPage}
            changePage={props.changePage}
          >
            My Books
          </MenuBtn>
          {/* </Link> */}
        </div>

        <div className={Styles.NoPad + " col-4 col-lg-12 my-2"}>
          {/* <Link to="/wish-books"> */}
          <MenuBtn
            type="wish-books"
            currentPage={props.currentPage}
            changePage={props.changePage}
          >
            Wish Books
          </MenuBtn>
          {/* </Link> */}
        </div>

        <div className={Styles.NoPad + " col-4 col-lg-12 my-2"}>
          {/* <Link to="/disliked-books"> */}
          <MenuBtn
            type="disliked-books"
            currentPage={props.currentPage}
            changePage={props.changePage}
          >
            Death List
          </MenuBtn>
          {/* </Link> */}
        </div>

        {/* <div className={Styles.NoPad + " col-4 col-lg-12"}>
          <Link to="/commented">
            <MenuBtn
              type="commented"
              currentPage={props.currentPage}
              changePage={props.changePage}
            >
              Commented
            </MenuBtn>
          </Link>
        </div>

        <div className={Styles.NoPad + " col-4 col-lg-12"}>
          <Link to="on-movies">
            <MenuBtn
              type="on-movies"
              currentPage={props.currentPage}
              changePage={props.changePage}
            >
              On Movies
            </MenuBtn>
          </Link>
        </div>

        <div className={Styles.NoPad + " col-4 col-lg-12"}>
          <Link to="on-amazon">
            <MenuBtn
              type="on-amazon"
              currentPage={props.currentPage}
              changePage={props.changePage}
            >
              On Amazon
            </MenuBtn>
          </Link>
        </div> */}
      </div>
    </div>
  );
};

export default Menu;
