import React from "react";

const NotFound = () => (
  <div>
    <h1 className="text-center my-5">4o4 ...</h1>
    <h1 className="text-center my-5">Opps! this page seems to be missing! </h1>
  </div>
);

export default NotFound;
