import React, { Component } from "react";
import ReactDOM from "react-dom";
import Styles from "./Overlay.module.css";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  closeOverlay,
  closeSearch,
  setCurrentPage,
  fetchSavedBooksFromDb
} from "../../config/actionCreators";
import CloseBtn from "../../assets/images/close.png";

import { history } from "../../App";

import { Card, CardHeader, CardBody, CardTitle } from "reactstrap";

class Overlay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isActive: true
    };
    this.overlayContainer = document.createElement("div");
    this.overlayContainer.className =
      "overlay-container d-flex justify-content-center align-items-center";
    this.overlayContainer.addEventListener("click", e => {
      this.shouldOverlayClose(e);
    });
  }

  componentDidMount() {
    document.body.appendChild(this.overlayContainer);
  }

  componentWillUnmount() {
    document.body.removeChild(this.overlayContainer);
  }

  shouldOverlayClose(e) {
    if (
      e.target.className.indexOf("overlay-container") > -1 ||
      e.target.className.indexOf("CloseBtn") > -1
    ) {
      this.backToMain();
    }
  }

  fadeOverlayOut() {
    this.setState({ isActive: false });
  }

  backToMain() {
    const {
      closeOverlay,
      closeSearch,
      currentPage,
      setCurrentPage,
      fetchSavedBooksFromDb
    } = this.props;

    this.fadeOverlayOut();

    let location = "/" + currentPage.split("/")[1];

    console.log(location, currentPage, "<==");
    setTimeout(() => {
      closeOverlay();
      closeSearch();
      setCurrentPage(location);
      fetchSavedBooksFromDb(location);
      history.push(location);
    }, 250);
  }

  render() {
    const { children, title } = this.props;

    return ReactDOM.createPortal(
      <div
        className={`${Styles.Overlay} animated ${
          this.state.isActive ? "fadeInLeftBig" : "fadeOutRightBig"
        } bg-dark`}
      >
        <Card className="h-100">
          <CardHeader>
            <div className="container">
              <div className="row">
                <div className="col-8">
                  <CardTitle className="m-0 mt-1">{title}</CardTitle>
                </div>
                <div className="col-4">
                  <span
                    onClick={e => this.shouldOverlayClose(e)}
                    className="float-right"
                  >
                    <img
                      src={CloseBtn}
                      alt="close button"
                      className={`img-fluid ${Styles.CloseBtn}`}
                    />
                  </span>
                </div>
              </div>
            </div>
          </CardHeader>
          <CardBody className={`${Styles.Scroll}`}>{children}</CardBody>
        </Card>
      </div>,
      this.overlayContainer
    );
  }
}

Overlay.propTypes = {
  closeOverlay: PropTypes.func.isRequired,
  wichAnimation: PropTypes.string.isRequired
};

const mapStateToProps = state => {
  return {
    wichAnimation: state.Overlay.wichAnimation,
    searchField: state.SearchState.searchField,
    currentPage: state.PageState.currentPage
  };
};

export default connect(
  mapStateToProps,
  { closeOverlay, closeSearch, setCurrentPage, fetchSavedBooksFromDb }
)(Overlay);
