import React, { Component } from "react";
import { Route, Redirect, withRouter } from "react-router-dom";
import { history } from "../../App";

import Menu from "../menu/Menu";
import Overlay from "../overlay/Overlay";
import SearchBox from "../searchBox/SearchBox";
import ErrorBoundary from "../errorBoundary/ErrorBoundary";

import { readableCurrentPage } from "../../config/utilFunctions";

import { connect } from "react-redux";

import {
  setCurrentPage,
  toggleSearch,
  closeSearch,
  openOverlay,
  closeOverlay,
  fetchSavedBooksFromDb,
} from "../../config/actionCreators";

class ProtectedRoute extends Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
    this.state = {
      fadePageIn: true,
    };
  }

  handleChangePage(page) {
    const { currentPage } = this.props.PageState;
    let location;

    if (page === "search" && currentPage.indexOf("search") === -1) {
      location = currentPage + "/search";
      this.props.toggleSearch(currentPage);
      this.props.openOverlay();
    } else if (page === "search" && currentPage.indexOf("search") > -1) {
      location = "/" + currentPage.split("/")[1];
      this.props.toggleSearch(currentPage);
      this.props.closeOverlay();
    } else {
      location = "/" + page;
      if (this.props.SearchState.isActive) this.props.closeSearch();
    }

    if (this.props.PageState.currentPage !== location) {
      // setTimeout(() => {
      this.props.setCurrentPage(location);
      // }, 0);
    }

    this.setState({ fadePageIn: false }, () => {
      setTimeout(() => {
        this.setState({ fadePageIn: true });
      }, 0);
    });
    this.props.fetchSavedBooksFromDb(location);
    setTimeout(() => {
      history.push(location);
    }, 0);
  }

  render() {
    const { loggedIn, path, rest, Component, PageState } = this.props;
    const isSearchActive = this.props.SearchState.isActive;
    let currentPage;
    if (PageState.currentPage)
      currentPage = PageState.currentPage.split("/")[1];
    return (
      <Route
        // path={path}
        // {...rest}
        {...this.props}
        render={(props) => {
          return loggedIn ? (
            <ErrorBoundary place="protectedroute">
              <React.Fragment>
                <div className={`row`}>
                  <div
                    className={`col-lg-9 pr-lg-0 books-component w-100 ${
                      this.state.fadePageIn
                        ? "animated fadeIn visible"
                        : "invisible"
                    }`}
                  >
                    <Component
                      {...this.props}
                      changePage={this.handleChangePage.bind(this)}
                      isSearchActive={isSearchActive}
                    />
                  </div>
                  <div className="col-lg-3 pl-lg-0">
                    <Menu
                      currentPage={this.props.PageState.currentPage}
                      changePage={this.handleChangePage.bind(this)}
                      isSearchActive={isSearchActive}
                    />
                  </div>
                </div>
                {this.props.Overlay.isOverlayActive && isSearchActive && (
                  <Overlay
                    title={readableCurrentPage(
                      PageState.currentPage
                    ).toUpperCase()}
                  >
                    <SearchBox />
                  </Overlay>
                )}
              </React.Fragment>
            </ErrorBoundary>
          ) : (
            <Redirect
              to={{
                pathname: "/login",
              }}
            />
          );
        }}
      />
    );
  }
}

const mapStateToProps = (state) => state;

const mapDispatchToProps = (state) => (dispatch) => {
  return {
    setCurrentPage: (page) => dispatch(setCurrentPage(page)),
    toggleSearch: (searchField) => dispatch(toggleSearch(searchField)),
    closeSearch: () => dispatch(closeSearch()),
    openOverlay: () => dispatch(openOverlay()),
    closeOverlay: () => dispatch(closeOverlay()),
    fetchSavedBooksFromDb: (bookField) =>
      dispatch(fetchSavedBooksFromDb(bookField)),
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ProtectedRoute)
);
