import React, { Component } from "react";
import { Button } from "reactstrap";
import { LocalForm } from "react-redux-form";
import { history } from "../../App";
import { connect } from "react-redux";
import {
  createUser,
  setCurrentPage,
  loginError
} from "../../config/actionCreators";
import throttle from "lodash.throttle";
import Input from "../input/Input";
import Spinner from "../spinner/Spinner";
import ErrorMessage from "../errorMessage/ErrorMessage";
import SuccessMessage from "../successMessage/SuccessMessage";

import { Redirect } from "react-router-dom";

import Styles from "./RegisterViewContainer.module.css";

class RegisterViewContainer extends Component {
  state = {
    password: "",
    repeatPassword: ""
  };

  componentDidMount = () => {
    this.props.setCurrentPage("register");

    if (!this.props.User.id && !this.props.User.errorMess)
      this.props.loginError(null);

    if (this.props.User.token) history.push("/my-books");
  };

  handleRegister = values => {
    let times = 0;
    console.log("fire");

    if (times === 0) this.props.createUser(values);
    times++;
  };

  setPass = e => {
    this.setState({ password: e.currentTarget.value });
  };

  setRepeatPass = e => {
    this.setState({ repeatPassword: e.currentTarget.value });
  };

  render() {
    const { User } = this.props;
    return !User.userId ? (
      <div className={Styles.FormContainer + " my-5 container"}>
        <h1 className="my-4">Register Here</h1>
        <LocalForm onSubmit={throttle(this.handleRegister, 2000)}>
          <Input inputType="first_Name" />

          <Input inputType="last_Name" />

          <Input inputType="email" />

          <Input inputType="password" setPass={this.setPass} />

          <Input
            inputType="repeatPassword"
            passToCompare={this.state.password}
            setRepeatPass={this.setRepeatPass}
          />
          <ErrorMessage {...User} />
          <SuccessMessage {...User} />
          <Button
            className="my-3"
            type="submit"
            value="submit"
            color="primary"
            size="lg"
          >
            Register Now
            <Spinner isActive={User.isLoading} />
          </Button>
        </LocalForm>
      </div>
    ) : (
      <Redirect to="/my-books" />
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => {
  return {
    createUser: value => dispatch(createUser(value)),
    loginError: values => dispatch(loginError(values)),
    setCurrentPage: page => dispatch(setCurrentPage(page))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterViewContainer);

// export default RegisterViewContainer;
