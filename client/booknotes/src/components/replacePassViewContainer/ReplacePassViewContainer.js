import React, { Component } from "react";
import { Button } from "reactstrap";
import { LocalForm } from "react-redux-form";
import { connect } from "react-redux";
import { history } from "../../App";
import { NavLink } from "react-router-dom";
import {
  sendReplacePassword,
  setCurrentPage,
  loginError
} from "../../config/actionCreators";
import Input from "../input/Input";
import Spinner from "../spinner/Spinner";
import ErrorMessage from "../errorMessage/ErrorMessage";
import SuccessMessage from "../successMessage/SuccessMessage";

import { Redirect } from "react-router-dom";

import Styles from "./ReplacePassViewContainer.module.css";

class LoginViewContainer extends Component {
  state = {
    password: "",
    repeatPassword: ""
  };

  componentDidMount = () => {
    this.props.setCurrentPage("replace-password");

    // if (!this.props.User.id) this.props.loginError(null);

    // if (this.props.User.token) history.push("/my-books");
  };

  setPass = e => {
    this.setState({ password: e.currentTarget.value });
  };

  setRepeatPass = e => {
    this.setState({ repeatPassword: e.currentTarget.value });
  };

  handleReplacePassword = password => {
    this.props.sendReplacePassword(password);
  };

  render() {
    const { User } = this.props;
    return !User.userId ? (
      <div className={Styles.FormContainer + " my-5 container"}>
        <h1 className="my-4">Replace your password</h1>
        <LocalForm onSubmit={this.handleReplacePassword}>
          <Input
            inputType="password"
            setPass={this.setPass}
            placeholder="type your old password..."
          />
          <Input
            inputType="repeatPassword"
            passToCompare={this.state.password}
            setRepeatPass={this.setRepeatPass}
            placeholder="type your new password..."
          />

          <ErrorMessage {...User} />
          <SuccessMessage {...User} />

          <Button
            className="my-3"
            type="submit"
            value="submit"
            color="primary"
            size="lg"
          >
            Change password
            <Spinner isActive={User.isLoading} />
          </Button>
        </LocalForm>
      </div>
    ) : (
      <Redirect to="/my-books" />
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => {
  return {
    sendReplacePassword: password => dispatch(sendReplacePassword(password)),
    loginError: values => dispatch(loginError(values)),
    setCurrentPage: page => dispatch(setCurrentPage(page))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginViewContainer);
