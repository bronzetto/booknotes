import React, { Component } from "react";
import { Button } from "reactstrap";
import { LocalForm } from "react-redux-form";
import { connect } from "react-redux";
import { history } from "../../App";
import { NavLink } from "react-router-dom";
import {
  sendChangePasswordRequest,
  setCurrentPage,
  loginError
} from "../../config/actionCreators";
import Input from "../input/Input";
import Spinner from "../spinner/Spinner";
import ErrorMessage from "../errorMessage/ErrorMessage";
import SuccessMessage from "../successMessage/SuccessMessage";

import { Redirect } from "react-router-dom";

import Styles from "./ResetPassViewContainer.module.css";

class LoginViewContainer extends Component {
  componentDidMount = () => {
    this.props.setCurrentPage("reset-password");

    // if (!this.props.User.id) this.props.loginError(null);

    // if (this.props.User.token) history.push("/my-books");
  };

  handleResetPassword = email => {
    this.props.sendChangePasswordRequest(email);
  };

  render() {
    const { User } = this.props;
    return !User.userId ? (
      <div className={Styles.FormContainer + " my-5 container"}>
        <h1 className="my-4">Forgot your password?</h1>
        <LocalForm onSubmit={this.handleResetPassword}>
          <Input inputType="email" placeholder="type ypur email here..." />

          <ErrorMessage {...User} />
          <SuccessMessage {...User} />

          <Button
            className="my-3"
            type="submit"
            value="submit"
            color="primary"
            size="lg"
          >
            Reset password
            <Spinner isActive={User.isLoading} />
          </Button>
        </LocalForm>
      </div>
    ) : (
      <Redirect to="/my-books" />
    );
  }
}

const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => {
  return {
    sendChangePasswordRequest: email =>
      dispatch(sendChangePasswordRequest(email)),
    loginError: values => dispatch(loginError(values)),
    setCurrentPage: page => dispatch(setCurrentPage(page))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginViewContainer);
