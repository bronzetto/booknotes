import { useEffect, useRef, useState } from "react";
import Styles from "./SearchBox.module.css";
import BookInSearch from "../bookInSearch/BookInSearch";
import { useSelector, useDispatch } from "react-redux";
import { fetchBooksBy, storeSelectedBook } from "../../config/actionCreators";
import { readableCurrentPage } from "../../config/utilFunctions";
import ErrorBoundary from "../errorBoundary/ErrorBoundary";

function SearchBox() {
  const [inputValue, setInputValue] = useState("");
  const textInput = useRef();
  const valueToFetch = useRef("");

  const dispatch = useDispatch();
  const {
    PageState,
    SearchState,
    dislikedBooks,
    myBooks,
    wishBooks,
  } = useSelector((state) => state);

  const { isActive, booksFound, fetchError } = SearchState;
  const { currentPage } = PageState;

  useEffect(() => {
    textInput.current.focus();
  }, []);

  useEffect(() => {
    const throttle = setTimeout(() => {
      const typedValue = inputValue.split("-").join("");
      const inputHasValue = typedValue.length > 0;
      const userTypingTitle = isNaN(typedValue);

      if (userTypingTitle) valueToFetch.current = typedValue;
      else if (!userTypingTitle) valueToFetch.current = `isbn:${typedValue}`;

      if (inputHasValue) dispatch(fetchBooksBy(valueToFetch.current));
    }, 600);

    return () => clearTimeout(throttle);
  }, [dispatch, inputValue]);

  const bookField = readableCurrentPage(PageState.currentPage)
    .split(" ")
    .join("");

  function handleInputText(e) {
    setInputValue(e.target.value);
  }

  function isCurrentSaved(id, active) {
    const preferences = { dislikedBooks, myBooks, wishBooks };
    return preferences[active].books.some((b) => b.googleId === id);
  }

  function handleSaveBook(book) {
    const bookField = PageState.currentPage;
    const basicInfoBook = {
      authors: book.volumeInfo.authors,
      categories: book.volumeInfo.categories,
      googleId: book.id,
      title: book.volumeInfo.title,
      image: book.volumeInfo.imageLinks.thumbnail,
      averageRating: book.volumeInfo.averageRating,
    };

    return dispatch(storeSelectedBook(basicInfoBook, bookField));
  }

  const container = isActive
    ? `${Styles.container} ${Styles.openContainer}`
    : `${Styles.container}`;

  return (
    <ErrorBoundary place="search">
      <div className={`${container} d-flex flex-column justify-content-center`}>
        <input
          className={`${Styles.BasicInput}`}
          placeholder="Type the book title or isbn number"
          ref={textInput}
          onChange={handleInputText}
          value={inputValue}
        />
        {fetchError && (
          <h3 className="mx-auto my-5 text-center">{fetchError}</h3>
        )}
        <div className="container my-4">
          <div className="row">
            {booksFound &&
              booksFound.map((book) => {
                return (
                  <BookInSearch
                    key={book.id}
                    book={book}
                    currentPage={currentPage}
                    isSaved={isCurrentSaved(book.id, bookField)}
                    handleSaveBook={(book) => handleSaveBook(book)}
                  />
                );
              })}
          </div>
        </div>
      </div>
    </ErrorBoundary>
  );
}

export default SearchBox;
