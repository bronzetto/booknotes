import React from "react";

const Spinner = ({ isActive }) => {
  //   return <span className="ml-3 fa fa-refresh fast-spin" />;
  return isActive ? <span className="ml-3 fa fa-refresh fast-spin" /> : null;
};

export default Spinner;
