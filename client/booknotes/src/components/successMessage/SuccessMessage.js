import React from "react";
import { Alert } from "reactstrap";
import Styles from "./SuccessMessage.module.css";

const ErrorMessage = ({ successMess }) => {
  return (
    <Alert
      className={!successMess ? Styles.HideAlert : Styles.ShowAlert}
      color="success"
    >
      {successMess}
    </Alert>
  );
};
export default ErrorMessage;
