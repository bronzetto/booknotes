import React, { Component } from "react";

import { withRouter } from "react-router-dom";

import { connect } from "react-redux";

import { setCurrentPage } from "../../config/actionCreators";

class WishBooks extends Component {
  componentDidMount() {
    if (this.props.PageState.currentPage !== "/wish-books")
      this.props.setCurrentPage("/wish-books");
  }
  render() {
    return (
      <div className="container">
        <div className="header border-bottom">
          <h1 className="text-center my-2">Wished Books</h1>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => state;

const mapDispatchToProps = state => dispatch => {
  return {
    setCurrentPage: page => dispatch(setCurrentPage(page))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(WishBooks)
);
