export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILED = "LOGIN_FAILED";
export const LOGOUT_REQUEST = "LOGOUT_REQUEST";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const LOGOUT_FAILED = "LOGOUT_FAILED";
export const REGISTER_REQUEST = "REGISTER_REQUEST";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAILED = "REGISTER_FAILED";
export const RESET_USER_MESSAGES = "RESET_USER_MESSAGES";
export const REQUEST_NEW_PASSWORD_REQUEST = "REQUEST_NEW_PASSWORD_REQUEST";
export const REQUEST_NEW_PASSWORD_SUCCESS = "REQUEST_NEW_PASSWORD_SUCCESS";
export const REQUEST_NEW_PASSWORD_FAILED = "REQUEST_NEW_PASSWORD_FAILED";
export const SET_PAGE = "SET_PAGE";
export const TOGGLE_SEARCH = "TOGGLE_SEARCH";
export const CLOSE_SEARCH = "CLOSE_SEARCH";
export const SET_SEARCH_TEXT = "SET_SEARCH_TEXT";
export const SET_SEARCH_FIELD = "SET_SEARCH_FIELD";
export const ACTIVATE_OVERLAY = "ACTIVATE_OVERLAY";
export const SET_BOOKS_FOUND_BY_SEARCH = "SET_BOOKS_FOUND_BY_SEARCH";
export const SET_FETCH_BOOKS_ERROR = "SET_FETCH_BOOKS_ERROR";

export const SAVE_BOOK_REQUEST_MYBOOKS = "SAVE_BOOK_REQUEST_MYBOOKS";
export const SAVE_BOOK_SUCCESS_MYBOOKS = "SAVE_BOOK_SUCCESS_MYBOOKS";
export const SAVE_BOOK_FAILED_MYBOOKS = "SAVE_BOOK_FAILED_MYBOOKS";
export const SAVE_BOOK_REQUEST_WISHBOOKS = "SAVE_BOOK_REQUEST_WISHBOOKS";
export const SAVE_BOOK_SUCCESS_WISHBOOKS = "SAVE_BOOK_SUCCESS_WISHBOOKS";
export const SAVE_BOOK_FAILED_WISHBOOKS = "SAVE_BOOK_FAILED_WISHBOOKS";
