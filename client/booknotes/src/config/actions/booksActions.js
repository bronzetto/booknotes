import * as Types from "../actionTypes";
import axios from "axios";
import store from "../../config/store";
import { getBaseUrl } from "../../config/utilFunctions";

export const getWereToSave = bookField => {
  // console.log("bookField===>", bookField);
  const field = bookField.split("/")[1];
  return field === "my-books"
    ? "myBooks"
    : field === "wish-books"
    ? "wishBooks"
    : field === "disliked-books"
    ? "dislikedBooks"
    : "";
};
const googleApiBaseUrl = `https://www.googleapis.com/books/v1/volumes`;
// `https://www.googleapis.com/books/v1/volumes?q="christmas"&key=${process.env.BOOK_API_KEY}`

let baseUrl = getBaseUrl();

const key = process.env.REACT_APP_BOOK_API_KEY;

export const fetchBooksBy = word => dispatch => {
  console.log(`${googleApiBaseUrl}?q=${word}&key=${key}`);
  if (word === "") {
    dispatch(setFetchError("Type something..."));
    return;
  }

  axios({
    method: "GET",
    url: `${googleApiBaseUrl}?q=${word}&maxResults=20&key=${key}`,
    headers: {
      "Content-Type": "application/json"
    }
  })
    .then(response => {
      let books = response.data.items;
      if (books && books.length > 0) {
        dispatch(setFound(books));
      } else {
        dispatch(setFetchError("No books found"));
      }
    })
    .catch(error => {
      console.error(error);
      dispatch(
        setFetchError(
          "Sorry there was a problem 😔 If you have used a ISBN number try to type the book title"
        )
      );
    });
};

export const setFound = books => {
  return {
    type: Types.SET_BOOKS_FOUND_BY_SEARCH,
    payload: { books }
  };
};

export const setFetchError = error => {
  return {
    type: Types.SET_FETCH_BOOKS_ERROR,
    payload: { error }
  };
};

export const storeSelectedBook = (book, bookField) => dispatch => {
  const { id: userId, token } = store.getState().User;

  let wereToSave = getWereToSave(bookField);

  let bookFieldType = bookField
    .split("/")[1]
    .split("-")
    .join("")
    .toUpperCase();

  dispatch(requestSaveBook(bookFieldType)());

  axios({
    method: "GET",
    url: `${baseUrl}api/${wereToSave}/${userId}`,
    headers: {
      "Content-Type": "application/json",
      Authorization: "bearer " + token
    }
    // data: JSON.stringify(book)
  })
    .then(response => {
      if (response.status === 200) {
        if (response.data.length > 0) {
          const thisBookInDb = response.data.filter(
            item => item.googleId === book.id
          );

          if (thisBookInDb.length === 1) {
            dispatch(successSaveBook(bookFieldType)(thisBookInDb[0]));
          } else if (thisBookInDb.length === 0) {
            saveBook(wereToSave, userId, token, dispatch, bookFieldType, book);
          }
        } else {
          saveBook(wereToSave, userId, token, dispatch, bookFieldType, book);
        }
      } else {
        const message = "OOPS!! response in not OK!";
        dispatch(failedSaveBook(bookFieldType, book)(message));
        throw new Error(message);
      }
    })
    .catch(error => {
      let message = "Sorry something went wrong, Couldn't save the Book! :(..";
      dispatch(failedSaveBook(bookFieldType, book)(message));
      console.error(error, message, "<==");
    });
};

export const saveBook = (
  wereToSave,
  userId,
  token,
  dispatch,
  bookFieldType,
  book
) => {
  axios({
    method: "PUT",
    url: `${baseUrl}api/${wereToSave}/${userId}`,
    headers: {
      "Content-Type": "application/json",
      Authorization: "bearer " + token
    },
    data: JSON.stringify(book)
  })
    .then(response => {
      if (response.status === 200) {
        dispatch(successSaveBook(bookFieldType)(book));
      } else {
        axios({
          method: "POST",
          url: `${baseUrl}api/${wereToSave}/${userId}`,
          headers: {
            "Content-Type": "application/json",
            Authorization: "bearer " + token
          },
          data: JSON.stringify(book)
        }).then(response => {
          if (response.status === 200) {
            dispatch(successSaveBook(bookFieldType)(book));
          } else {
            const message = "OOPS!! propblem in post";
            dispatch(failedSaveBook(bookFieldType, book)(message));
            throw new Error(message);
          }
        });
      }
    })
    .catch(error => {
      console.log(error);
    });
};

export const requestSaveBook = bookField => () => {
  return {
    type: `SAVE_BOOK_REQUEST_${bookField}`
  };
};

export const failedSaveBook = (bookField, book) => errorMessage => {
  return {
    type: `SAVE_BOOK_FAILED_${bookField}`,
    payload: {
      errorMessage: errorMessage,
      book: book
    }
  };
};

export const successSaveBook = bookField => newBook => {
  return {
    type: `SAVE_BOOK_SUCCESS_${bookField}`,
    payload: {
      successMessage: "Book successful saved! :)",
      newBook: newBook
    }
  };
};

export const fetchSavedBooksFromDb = bookField => dispatch => {
  const { id: userId, token } = store.getState().User;
  let wereToSave = getWereToSave(bookField);
  let bookFieldType = bookField
    .split("/")[1]
    .split("-")
    .join("")
    .toUpperCase();

  dispatch(requestSaveBook(bookFieldType)());
  axios({
    method: "GET",
    url: `${baseUrl}api/${wereToSave}/${userId}`,
    headers: {
      "Content-Type": "application/json",
      Authorization: "bearer " + token
    }
  })
    .then(response => {
      if (response.status === 200) {
        // update Store
        dispatch(successGettingAllBooks(bookFieldType)(response.data));
      } else {
        const message = "OOPS!! response in not OK!";
        throw new Error(message);
      }
    })
    .catch(error => {
      console.log(error);
    });
};

export const successGettingAllBooks = bookField => allBooks => {
  return {
    type: `SAVE_ALL_BOOKS_${bookField}`,
    payload: { allBooks }
  };
};

// Delete book
export const deleteCurrentBook = (bookField, book) => dispatch => {
  const { id: userId, token } = store.getState().User;
  let wereToSave = getWereToSave("/" + bookField);
  let bookFieldType = bookField
    .split("-")
    .join("")
    .toUpperCase();

  // deleting request
  dispatch(requestDeleteBook(bookFieldType));
  axios({
    method: "DELETE",
    url: `${baseUrl}api/${wereToSave}/${userId}`,
    headers: {
      "Content-Type": "application/json",
      Authorization: "bearer " + token
    },
    data: JSON.stringify({ id: book.googleId })
  })
    .then(response => {
      if (response.status === 200) {
        dispatch(successDeleteBook(bookFieldType, book)("Book deleted!"));
      } else {
        const message = "OOPS!! response in not OK!";
        throw new Error(message);
      }
    })
    .catch(error => {
      console.log(error);
      dispatch(errorDeleteBook(bookFieldType)("An error occurred!"));
    });
};

export const requestDeleteBook = bookField => {
  return {
    type: `REQUEST_DELETE_BOOKS_${bookField}`
  };
};

export const successDeleteBook = (bookField, book) => successMessage => {
  return {
    type: `SUCCESS_DELETE_BOOKS_${bookField}`,
    payload: { successMessage, book }
  };
};

export const errorDeleteBook = bookField => errorMessage => {
  return {
    type: `ERROR_DELETE_BOOKS_${bookField}`,
    payload: { errorMessage }
  };
};
