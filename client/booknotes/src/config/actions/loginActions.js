import * as Types from "../actionTypes";
import axios from "axios";
import { history } from "../../App";
import { storageAvailable, getBaseUrl } from "../utilFunctions";

const baseUrl = getBaseUrl();

export const createUser =
  ({ password, first_Name, last_Name, email }) =>
  (dispatch) => {
    let user = {
      password: password,
      firstName: first_Name,
      lastName: last_Name,
      username: email,
    };

    dispatch(requestRegister());
    axios({
      method: "POST",
      url: `${baseUrl}users/signup`,
      headers: {
        "Content-Type": "application/json",
      },
      data: JSON.stringify(user),
    })
      .then((response) => {
        let user = response.data;
        return user;
      })
      .then((user) => {
        if (storageAvailable("localStorage")) {
          localStorage.setItem("user", JSON.stringify(user));
        } else {
          console.log("Local storage is not supported in this browser");
        }

        dispatch(
          receiveRegistration(
            "Almost done!!! We've sent you and email with a link to click to activate your account!"
          )
        );
      })
      .catch((error) => {
        if (error.response) {
          console.error(error.response);
          const { status } = error.response;

          if (status === 500) {
          } else {
            let { err } = error.response.data;
            const newMess = err.message.replace("username", "email");
            dispatch(RegisterError(newMess));
          }
        } else {
          console.log(error);
          dispatch(RegisterError("Sorry, an error occurred!"));
        }
      });
  };

export const requestRegister = () => {
  return {
    type: Types.REGISTER_REQUEST,
  };
};

export const receiveRegistration = (successMess) => {
  return {
    type: Types.REGISTER_SUCCESS,
    payload: successMess,
  };
};

export const RegisterError = (errorMess) => {
  return {
    type: Types.REGISTER_FAILED,
    payload: errorMess,
  };
};

// =================== LOGIN =====================

export const loginUser = (creds) => (dispatch) => {
  dispatch(requestLogin());

  axios({
    method: "POST",
    url: `${baseUrl}users/login`,
    headers: {
      "Content-Type": "application/json",
    },
    data: JSON.stringify({
      username: creds.email,
      password: creds.password,
    }),
  })
    .then((response) => {
      let user = response.data.user;
      return user;
    })
    .then((user) => {
      if (storageAvailable("localStorage")) {
        localStorage.setItem("user", JSON.stringify(user));
      } else {
        console.log("Local storage is not supported in this browser");
      }

      dispatch(receiveLogin(user));
    })
    .then(() => {
      history.push("/my-books");
    })
    .catch((error) => {
      if (error.response) {
        let { err } = error.response.data;
        console.error(err);
        dispatch(loginError(err.message));
      } else {
        console.error(error);
        dispatch(loginError("I wasn't able to log you in..."));
      }
    });
};

export const requestLogin = () => {
  return {
    type: Types.LOGIN_REQUEST,
  };
};

export const receiveLogin = (user) => {
  return {
    type: Types.LOGIN_SUCCESS,
    payload: user,
  };
};

export const loginError = (errorMess) => {
  return {
    type: Types.LOGIN_FAILED,
    payload: errorMess,
  };
};

// check if user is logged and in localStorage checkJwt
// and if other error and redirect from the server
export const checkIfLogged = (user) => (dispatch) => {
  const isThereRedirectFromServer = window.location.href.indexOf("?") > -1;
  //if (true) {
  let upcomingUrl = window.location.href.split("?");
  let queryPath = upcomingUrl[1] || "";
  if (queryPath.indexOf("registeredUnsuccessfull") > -1) {
    let currentError = queryPath.split("=")[1];
    let error =
      currentError === "UserExistsError"
        ? "A user with your credentials already exists"
        : currentError === "TokenExpiredError"
        ? "The token has expired! Try to Register again."
        : "An error occured. Try to register again";
    history.push(`/register`);
    setTimeout(() => {
      dispatch(RegisterError(error));
    }, 1000);
  } else if (queryPath.indexOf("passwordChangeUnsuccessfull") > -1) {
    let error;
    if (queryPath.split("=")[1] === "TokenExpiredError") {
      error = "The token has expired. You can require another password change";
    } else {
      error = "There was an error with your token. Please Require another one";
    }
    history.push(`/reset-password`);
    dispatch(loginError(error));
  } else if (queryPath.indexOf("registeredSuccessfull") > -1) {
    history.push("/login");
    setTimeout(() => {
      dispatch(
        receiveRegistration(
          "You have been successufully registered. You can now Log in"
        )
      );
    }, 1000);
  } else {
    history.push(`/${queryPath}`);
  }
  //}

  if (storageAvailable("localStorage") && !isThereRedirectFromServer) {
    if (window.localStorage.getItem("user")) {
      let user = JSON.parse(window.localStorage.getItem("user"));
      dispatch(requestLogin(user));

      axios({
        method: "GET",
        url: `${baseUrl}users/checkJwt`,
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${user.token}`,
        },
      })
        .then((response) => {
          let token = JSON.parse(window.localStorage.getItem("user")).token;
          let user = response.data.user;
          user = {
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.username,
            id: user._id,
            token: token,
          };

          return user;
        })
        .then((user) => {
          localStorage.setItem("user", JSON.stringify(user));

          dispatch(receiveLogin(user));
        })
        .then(() => {
          let upcomingUrl = window.location.href.split("?");
          let queryPath = upcomingUrl[1] || "";

          console.log("ADSFAFAD3", queryPath);
          history.push(`/${queryPath}`);
        })
        .catch((error) => {
          console.error(error);
          // dispatch(
          //   loginError(
          //     "there was a problem with the authentication. Try to log in"
          //   )
          // );
          history.push(`/login`);
        });
    }
  } else {
    console.log("Local storage is not supported in this browser");
  }
};

// Logs the user out
export const logoutUser = () => (dispatch) => {
  dispatch(requestLogout());

  if (storageAvailable("localStorage")) {
    localStorage.removeItem("user");
  }

  dispatch(receiveLogout());
};

export const requestLogout = () => {
  return {
    type: Types.LOGOUT_REQUEST,
  };
};

export const receiveLogout = () => {
  return {
    type: Types.LOGOUT_SUCCESS,
  };
};

export const logoutError = (errorMess) => {
  return {
    type: Types.LOGOUT_FAILED,
    payload: errorMess,
  };
};

// CHANGE PASSWORD
export const sendChangePasswordRequest = (email) => (dispatch) => {
  dispatch(requestChangePass());

  axios({
    method: "POST",
    url: `${baseUrl}users/request-reset-password`,
    headers: {
      "Content-Type": "application/json",
    },
    data: JSON.stringify({ email }),
  })
    .then((response) => {
      dispatch(
        receiveChangePass(
          "We have Sent you an email with a link to reset your password"
        )
      );
    })
    .catch((error) => {
      console.error(error);
      dispatch(changePassError("I wasn't able to process the request..."));
    });
};

export const requestChangePass = () => {
  return {
    type: Types.REQUEST_NEW_PASSWORD_REQUEST,
  };
};

export const receiveChangePass = (successMess) => {
  return {
    type: Types.REQUEST_NEW_PASSWORD_SUCCESS,
    payload: successMess,
  };
};

export const changePassError = (errorMess) => {
  return {
    type: Types.REQUEST_NEW_PASSWORD_FAILED,
    payload: errorMess,
  };
};

export const sendReplacePassword =
  ({ password, repeatPassword }) =>
  (dispatch) => {
    const splittedUrl = window.location.href.split("/");
    const token = splittedUrl[splittedUrl.length - 1];
    console.log(
      "EMAIL PASSWORD TO CHANGE: ",
      baseUrl,
      "------",
      token,
      password,
      repeatPassword
    );

    // send post request
    dispatch(requestChangePass());

    axios({
      method: "POST",
      url: `${baseUrl}users/reset-password/${token}`,
      headers: {
        "Content-Type": "application/json",
      },
      data: JSON.stringify({ newPassword: password }),
    })
      .then((response) => {
        history.push("/login");
        setTimeout(() => {
          dispatch(
            receiveChangePass("Your new password is set! you can now login! ")
          );
        }, 1000);
      })
      .catch((error) => {
        console.error("==>", { ...error });
        let mess;
        if (error.response && error.response.data && error.response.data.err) {
          mess = error.response.data.err.message;
        } else {
          mess = error.response.statusText;
        }
        dispatch(
          changePassError(mess + ". I wasn't able to process the request...")
        );
      });
  };
