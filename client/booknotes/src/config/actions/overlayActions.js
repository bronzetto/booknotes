import * as Types from "../actionTypes";

// ===================== OVERLAY  =====================

export const closeOverlay = () => dispatch => {
  dispatch({
    type: Types.ACTIVATE_OVERLAY,
    payload: {
      isActiveOverlay: false,
      wichAnimation: ""
    }
  });
};

export const openOverlay = () => dispatch => {
  dispatch({
    type: Types.ACTIVATE_OVERLAY,
    payload: {
      isActiveOverlay: true,
      wichAnimation: "fadeInLeftBig"
    }
  });
};
