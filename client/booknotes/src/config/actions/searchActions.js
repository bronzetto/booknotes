import * as Types from "../actionTypes";

// =================== SEARCH =====================

export const toggleSearch = searchField => dispatch => {
  dispatch({
    type: Types.TOGGLE_SEARCH,
    payload: { searchField }
  });
};

export const closeSearch = () => dispatch => {
  dispatch({
    type: Types.CLOSE_SEARCH
  });
};
