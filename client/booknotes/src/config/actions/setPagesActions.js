import * as Types from "../actionTypes";

// SET CURRENT PAGE
export const setCurrentPage = page => dispatch => {
  dispatch(setPage(page));
  dispatch(resetMessages());
};

export const setPage = page => {
  return {
    type: Types.SET_PAGE,
    payload: page
  };
};

export const resetMessages = () => {
  return {
    type: Types.RESET_USER_MESSAGES
  };
};
