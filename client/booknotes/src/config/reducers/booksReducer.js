// import * as Types from "../actionTypes";

const initializeState = {
  books: [],
  areLoading: false,
  isSaving: false,
  isDeleting: false,
  successMessage: null,
  error: {
    message: null,
    book: []
  }
};

export const BooksReducer = bookField => (state = initializeState, action) => {
  switch (action.type) {
    case `SAVE_BOOK_REQUEST_${bookField}`:
      return {
        ...state,
        isSaving: true,
        successMessage: null,
        error: {
          message: null,
          book: []
        }
      };
    case `SAVE_BOOK_SUCCESS_${bookField}`:
      const { books } = state;
      // const bookIfnotInAlready = book => {
      //   const match = state.books.filter(book => {
      //     book.id
      //   })
      // }
      return {
        ...state,
        isSaving: false,
        successMessage: action.payload.successMessage,
        error: {
          message: null,
          book: []
        },
        books: [...books, action.payload.newBook]
      };
    case `SAVE_BOOK_FAILED_${bookField}`:
      return {
        ...state,
        isSaving: false,
        error: {
          ...state.error,
          message: action.payload.errorMessage,
          book: action.payload.book
        },
        successMessage: null
      };
    case `SAVE_ALL_BOOKS_${bookField}`:
      return {
        ...state,
        isSaving: false,
        error: {
          message: null,
          book: []
        },
        books: [...action.payload.allBooks]
      };

    case `REQUEST_DELETE_BOOKS_${bookField}`:
      return {
        ...state,
        isDeliting: true
      };

    case `SUCCESS_DELETE_BOOKS_${bookField}`:
      return {
        ...state,
        isDeliting: false,
        successMessage: action.payload.successMessage,
        books: [
          ...state.books.filter(book => book._id !== action.payload.book._id)
        ],
        error: {
          message: null
        }
      };

    case `ERROR_DELETE_BOOKS_${bookField}`:
      return {
        ...state,
        isDeliting: false,
        successMessage: null,
        error: {
          message: action.payload.errorMessage
        }
      };

    default:
      return state;
  }
};
