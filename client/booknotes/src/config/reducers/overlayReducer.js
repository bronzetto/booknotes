import { ACTIVATE_OVERLAY } from "../actionTypes";

const initializeState = {
  isOverlayActive: false,
  animateInOut: ""
};

export const OverlayReducer = (state = initializeState, action) => {
  switch (action.type) {
    case ACTIVATE_OVERLAY:
      return {
        ...state,
        isOverlayActive: action.payload.isActiveOverlay,
        wichAnimation: action.payload.wichAnimation
      };

    default:
      return state;
  }
};
