import * as Types from "../actionTypes";

const initialState = {
  currentPage: null
};

export const PageStateReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.SET_PAGE:
      return {
        ...state,
        currentPage: action.payload
      };

    default:
      return state;
  }
};
