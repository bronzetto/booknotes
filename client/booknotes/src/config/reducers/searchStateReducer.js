import * as Types from "../actionTypes";

const initialState = {
  isActive: false,
  searchField: null, //my-books, wish-books ...
  searchText: null,
  booksFound: null,
  fetchError: null
};

export const SearchStateReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.TOGGLE_SEARCH:
      return {
        ...state,
        isActive: !state.isActive,
        searchField: !state.isActive
          ? action.payload.searchField.split("/")[1]
          : null
      };

    case Types.CLOSE_SEARCH:
      return {
        ...state,
        isActive: false,
        searchField: null,
        searchText: null,
        booksFound: null,
        fetchError: null
      };

    case Types.SET_SEARCH_TEXT:
      return {
        ...state,
        isActive: action.payload.isActive,
        searchText: action.payload.searcText,
        searchField: action.payload.searchField,
        fetchError: null
      };

    case Types.SET_BOOKS_FOUND_BY_SEARCH:
      return {
        ...state,
        booksFound: [...action.payload.books],
        fetchError: null
      };

    case Types.SET_FETCH_BOOKS_ERROR:
      return {
        ...state,
        fetchError: action.payload.error,
        booksFound: null
      };

    default:
      return state;
  }
};
