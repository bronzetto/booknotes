import * as Types from "../actionTypes";

const initialState = {
  firstName: null,
  lastName: null,
  email: null,
  id: null,
  token: null,
  errorMess: null,
  successMess: null,
  isLoading: false
};

export const UserReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.RESET_USER_MESSAGES:
      return {
        ...state,
        isLoading: false,
        errorMess: null,
        successMess: null
      };

    case Types.REGISTER_REQUEST:
      return {
        ...state,
        isLoading: true,
        errorMess: null,
        successMess: null
      };

    case Types.REGISTER_SUCCESS:
      return {
        ...state,
        successMess: action.payload,
        errorMess: null,
        isLoading: false
      };

    case Types.REGISTER_FAILED:
      return {
        ...state,
        errorMess: action.payload,
        successMess: null,
        isLoading: false
      };

    case Types.REQUEST_NEW_PASSWORD_REQUEST:
      return {
        ...state,
        isLoading: true,
        errorMess: null,
        successMess: null
      };

    case Types.REQUEST_NEW_PASSWORD_SUCCESS:
      return {
        ...state,
        successMess: action.payload,
        errorMess: null,
        isLoading: false
      };

    case Types.REQUEST_NEW_PASSWORD_FAILED:
      return {
        ...state,
        errorMess: action.payload,
        successMess: null,
        isLoading: false
      };

    case Types.LOGIN_REQUEST:
      return {
        ...state,
        isLoading: true,
        errorMess: null,
        successMess: null
      };

    case Types.LOGIN_SUCCESS:
      return {
        ...state,
        firstName: action.payload.firstName,
        lastName: action.payload.lastName,
        email: action.payload.email,
        id: action.payload.id,
        token: action.payload.token,
        errorMess: null,
        successMess: null,
        isLoading: false
      };

    case Types.LOGIN_FAILED:
      return {
        ...state,
        errorMess: action.payload,
        successMess: null,
        isLoading: false
      };

    case Types.LOGOUT_FAILED:
      return {
        ...state,
        errorMess: action.payload,
        successMess: null
      };

    default:
      return state;
  }
};
