import { createStore, applyMiddleware, combineReducers, compose } from "redux";
import thunk from "redux-thunk";

import { UserReducer } from "./reducers/userReducer";
import { PageStateReducer } from "./reducers/pageStateReducer";
import { SearchStateReducer } from "./reducers/searchStateReducer";
import { OverlayReducer } from "./reducers/overlayReducer";
import { BooksReducer } from "./reducers/booksReducer";
import { LOGOUT_REQUEST } from "./actionTypes";

const initialState = {};

const middleware = [thunk];

const mode = process.env.NODE_ENV;

const appReducer = combineReducers({
  User: UserReducer,
  PageState: PageStateReducer,
  SearchState: SearchStateReducer,
  Overlay: OverlayReducer,
  myBooks: BooksReducer("MYBOOKS"),
  wishBooks: BooksReducer("WISHBOOKS"),
  dislikedBooks: BooksReducer("DISLIKEDBOOKS")
});

// On logout we need to reset the state variable to reset all state.
// note that we are just resetting a varible and not mutating the state
// wich is not permitted. See here ==> https://stackoverflow.com/questions/35622588/how-to-reset-the-state-of-a-redux-store/35641992#35641992
const rootReducer = (state, action) => {
  if (action.type === LOGOUT_REQUEST) {
    state = undefined;
  }
  return appReducer(state, action);
};

// const reduxDevToolOrNot = () => {
//   return mode === "development" // use it just in chrome for less problems!
//     ? window.__REDUX_DEVTOOLS_EXTENSION__ &&
//         window.__REDUX_DEVTOOLS_EXTENSION__({ trace: true })
//     : null;
// };

const composeEnhancers =
  typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        trace: true
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
      })
    : compose;

const store = createStore(
  rootReducer,
  initialState,
  composeEnhancers(applyMiddleware(...middleware))
  // other store enhancers if any
);

export default store;
