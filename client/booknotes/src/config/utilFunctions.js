export const required = (val) => val && val.length;
export const maxLength = (len) => (val) => !val || val.length <= len;
export const minLength = (len) => (val) => val && val.length >= len;
export const isNumber = (val) => !isNaN(Number(val));
export const validEmail = (val) =>
  /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val);
export const validPassword = (pass) => (repeatedPass) =>
  repeatedPass && repeatedPass === pass;

export const capitalizeOne = (word) => {
  typeof word === Array ? (word = word[0]) : (word = word);
  return (
    word.split("").splice(0, 1)[0].toUpperCase() +
    word
      .split("")
      .splice(1, word.length - 1)
      .join("")
  );
};
// takes url and return readable string to insert as title
export const readableCurrentPage = (url) => {
  let parsedUrl = url.split("/")[1];
  parsedUrl = parsedUrl.split("-") || [parsedUrl];

  return parsedUrl.length > 1
    ? parsedUrl[0] + " " + capitalizeOne(parsedUrl[1])
    : capitalizeOne(parsedUrl[0]);
};

// Get exerpt similar to wordrpress function
export const getExerpt = (str, len) => {
  let exerpt = str.split("").slice(0, len);
  let finalTitle = exerpt.join("");
  return str.length > len ? `${finalTitle}...` : finalTitle;
};

// get if localstorage is supported in current browser
export const storageAvailable = (type) => {
  try {
    var storage = window[type],
      x = "__storage_test__";
    storage.setItem(x, x);
    storage.removeItem(x);
    return true;
  } catch (e) {
    return (
      e instanceof DOMException &&
      // everything except Firefox
      (e.code === 22 ||
        // Firefox
        e.code === 1014 ||
        // test name field too, because code might not be present
        // everything except Firefox
        e.name === "QuotaExceededError" ||
        // Firefox
        e.name === "NS_ERROR_DOM_QUOTA_REACHED") &&
      // acknowledge QuotaExceededError only if there's something already stored
      storage.length !== 0
    );
  }
};

export const getBaseUrl = () => {
  let baseUrl;
  if (
    process.env.NODE_ENV === "development" ||
    window.location.href.indexOf("localhost") > -1
  )
    baseUrl = "http://localhost:3005/";
  if (
    process.env.NODE_ENV === "production" &&
    window.location.href.indexOf("localhost") === -1
  )
    baseUrl = "https://my-book-notes-staging.herokuapp.com/";

  return baseUrl;
};
