const helpers = {};

helpers.notSupported = (req, res) => {
  res.statusCode = 403;
  res.end(
    `${req.method.toUpperCase()} operation not supported on ${req.originalUrl}`
  );
};

helpers.ok = (req, res) => {
  res.statusCode = 200;
  res.end(
    `${req.method.toUpperCase()} operation is GOOD TO GO on ${req.originalUrl}`
  );
};

helpers.doneAndSendPayload = (res, payload) => {
  res.statusCode = 200;
  res.setHeader("Content-Type", "application/json");
  res.json(payload);
};

helpers.bookAlreadyInDatabase = (book, next) => {
  err = new Error(
    "Conflict: book " + book.title + " is already in database \n" + book
  );
  err.status = 423;
  return next(err);
};

helpers.UserAlreadyInOwners = (book, user, next) => {
  err = new Error(
    "Conflict: user " +
      user +
      " has already the book in the personal list " +
      book
  );
  err.status = 423;
  return next(err);
};

helpers.UserAlreadyExists = (res, user, next) => {
  err = new Error("Conflict: user " + user + " Already Exists ");
  res.send("User Already Exists");
  err.status = 409;
  return next(err);
};

helpers.bookOrUserNotFound = (book, user, next) => {
  err = new Error(
    "Conflict: user " +
      user +
      " or book " +
      book.title +
      " not found in the personal list "
  );
  err.status = 404;
  return next(err);
};

helpers.UserNotFound = (user, next) => {
  err = new Error(user + " doesn't seem to exist");
  err.status = 404;
  return next(err);
};

helpers.bookNotFound = (book, next, res) => {
  err = new Error("book " + book + " not found");
  err.status = 404;
  return next(err);
  // res.statusCode = 200;
  // res.setHeader("Content-Type", "application/json");
  // return json([]);
};

helpers.logInSuccess = (res, user, token) => {
  res.statusCode = 200;
  res.setHeader("Content-Type", "application/json");
  res.json({
    success: true,
    token: token,
    user: user,
    status: "You are successfully logged in!"
  });
};

helpers.logInFailed = (res, err) => {
  res.statusCode = 401;
  res.setHeader("Content-Type", "application/json");
  res.json({
    success: false,
    status: "Login unsuccessful!",
    err: err
  });
};

helpers.jwtInvalid = (res, info) => {
  res.statusCode = 401;
  res.setHeader("Content-Type", "application/json");
  res.json({
    success: false,
    err: info,
    status: "Jwt is invalid!"
  });
};

helpers.jwtValid = (res, user) => {
  res.statusCode = 200;
  res.setHeader("Content-Type", "application/json");
  res.json({
    success: true,
    user: user,
    status: "Jwt is valid!"
  });
};

module.exports = helpers;
