const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userRefSchema = new Schema({
  username: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  }
});

const bestBookSchema = new Schema(
  {
    googleId: {
      type: String,
      required: true
    },
    title: {
      type: String,
      required: true
    },
    authors: {
      type: [String],
      required: true
    },
    categories: {
      type: [String],
      required: true
    },
    image: {
      type: String,
      required: true
    },
    averageRating: {
      type: Number,
      required: false,
      min: 0
    },
    userOwners: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: false
      }
    ]
  },
  {
    timestamps: true
  }
);

var bestBooks = mongoose.model("bestBook", bestBookSchema);

module.exports = bestBooks;
