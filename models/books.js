const mongoose = require("mongoose");
const Schema = mongoose.Schema;
require("mongoose-currency").loadType(mongoose);
const Currency = mongoose.Types.Currency;

const priceSchema = new Schema({
  amount: {
    type: Currency,
    require: false,
    min: 0
  },
  currencyCode: {
    type: String,
    require: false
  }
});

const saleInfoSchema = new Schema({
  country: {
    type: String,
    require: false
  },
  saleability: {
    type: String,
    require: false
  },
  isEbook: {
    type: String,
    require: false
  },
  listPrice: priceSchema,
  retailPrice: priceSchema,
  buyLink: {
    type: String,
    require: false
  }
});

const imageLinksSchema = new Schema({
  smallThumbnail: {
    type: String,
    required: false
  },
  thumbnail: {
    type: String,
    required: false
  }
});

const identifiersSchema = new Schema({
  type: {
    type: String,
    required: false
  },
  identifier: {
    type: String,
    required: false
  }
});

const volumeInfoSchema = new Schema({
  title: {
    type: String,
    required: false
  },
  subtitle: {
    type: String,
    required: false
  },
  authors: {
    type: [String],
    required: false
  },
  averageRating: {
    type: Number,
    required: false
  },
  categories: {
    type: [String],
    required: false
  },
  publisher: {
    type: String
    // required: false
  },
  publishedDate: {
    type: String,
    required: false
  },
  description: {
    type: String
    // required: false
  },
  imageLinks: [imageLinksSchema],
  industryIdentifiers: [identifiersSchema]
});

const bookSchema = new Schema(
  {
    id: {
      type: String,
      required: false
    },
    etag: {
      type: String,
      required: false
    },
    selfLink: {
      type: String,
      required: false
    },
    pageCount: {
      type: Number,
      required: false
    },
    printType: {
      type: String,
      required: false
    },
    previewLink: {
      type: String,
      required: false
    },
    infoLink: {
      type: String,
      required: false
    },
    canonicalVolumeLink: {
      type: String,
      required: false
    },
    language: {
      type: String,
      required: false
    },
    imageLinks: imageLinksSchema,
    volumeInfo: volumeInfoSchema,
    saleInfo: saleInfoSchema
  },
  {
    timestamps: true
  }
);

var books = mongoose.model("book", bookSchema);

module.exports = books;
