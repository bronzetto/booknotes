const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const commentSchema = new Schema(
  {
    myRating: {
      type: Number,
      min: 0,
      max: 5,
      required: false
    },
    comment: {
      type: String,
      required: true
    },
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true
    },
    bookRef: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "book",
      required: true
    },
    commentRef: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "comment",
      required: false
    }
  },
  {
    timestamps: true
  }
);

var comments = mongoose.model("comment", commentSchema);

module.exports = comments;
