const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const passportLocalMongoose = require("passport-local-mongoose");

const User = new Schema({
  firstName: {
    type: String,
    required: true,
    default: ""
  },
  lastName: {
    type: String,
    required: true,
    default: ""
  },
  admin: {
    type: Boolean,
    default: false
  }
});

// User.path("email").validate(function(email) {
//   var emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
//   return emailRegex.test(email); // Assuming email has a text attribute
// }, "The e-mail is not a valid");

User.plugin(passportLocalMongoose);

module.exports = mongoose.model("User", User);
