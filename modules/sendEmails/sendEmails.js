"use strict";
const nodemailer = require("nodemailer");

async function main({ type, verificationUrl, userDestination }, res) {
  let transporter = nodemailer.createTransport({
    host: "smtp.eu.mailgun.org",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: "postmaster@email.bookshelfy.com",
      pass: "e21ef7c09860a8bbaea9258b26a6d487-1b65790d-8b32d44e"
    }
  });

  let emailBody =
    type === "email-verification"
      ? `<h1>Welcome to Bookshelfy!</h1>  <p>click here to activate your account: </p><a href="${verificationUrl}">${verificationUrl}</a>`
      : type === "password-change"
      ? `<h1>Welcome back to Bookshelfy!</h1>  <p>Someone asked to reset the password associated to this email. If this is you, click the link to reset your password. If that wasn't you, just ignore this email: </p><a href="${verificationUrl}">${verificationUrl}</a>`
      : "";

  console.log(emailBody, userDestination, "<==");
  // setup email data with unicode symbols
  let mailOptions = {
    from: '"Bookshelfy.com 📚📚📚📚📚📚 " <postmaster@email.bookshelfy.com>',
    to: userDestination, // list of receivers
    subject: "Activate your Bookshelfy account ✔", // Subject line
    // text: "Hello world?", // plain text body
    html: emailBody // html body
  };

  // send mail with defined transport object
  let info = await transporter.sendMail(mailOptions);

  res.statusCode = 200;
  res.setHeader("Content-Type", "application/json");
  res.json({ success: true, status: "Confirmation email sent" });
}

module.exports = main;
