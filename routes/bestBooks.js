const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
mongoose.set("debug", true);
const helpers = require("../helperFunc");
const bestBooksRouter = express.Router();

const bestBooks = require("../models/bestBooks");
const books = require("../models/books");

const authenticate = require("../authenticate");

const cors = require("./cors");

bestBooksRouter.use(bodyParser.json());

bestBooksRouter
  .route("/")
  .get(authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    bestBooks
      .find({})
      .then(
        bestBooks => helpers.doneAndSendPayload(res, bestBooks),
        err => next(err)
      )
      .catch(err => next(err));
  })
  .post(authenticate.verifyUser, (req, res, next) => {
    helpers.notSupported(req, res);
  })
  .put(authenticate.verifyUser, (req, res, next) => {
    helpers.notSupported(req, res);
  })
  .delete(authenticate.verifyUser, (req, res, next) => {
    helpers.notSupported(req, res);
  });

bestBooksRouter
  .route("/:userId")
  .get(authenticate.verifyUser, (req, res, next) => {
    let book = req.body;
    bestBooks
      .find({})
      .then(
        bestBook => {
          let response = [];
          bestBook.forEach(el => {
            if (el.userOwners.indexOf(req.params.userId) > -1) {
              response.push(el);
            }
          });
          return response;
        },
        err => next(err)
      )
      .then(response => {
        helpers.doneAndSendPayload(res, response);
      })
      .catch(err => next(err));
  })
  .post(authenticate.verifyUser, (req, res, next) => {
    let book = req.body;
    bestBooks
      .findOne({ googleId: book.id }) // find in your best books (our userId)
      .then(
        bestBook => {
          if (bestBook == null) {
            let thisImage = book.volumeInfo.imageLinks.thumbnail || "";
            let newBestBook = {
              googleId: book.id,
              title: book.volumeInfo.title,
              authors: book.volumeInfo.authors,
              categories: book.volumeInfo.categories,
              image: thisImage,
              averageRating: book.volumeInfo.averageRating,
              userOwners: req.params.userId
            };
            bestBooks.create(newBestBook).then(bestBook => {
              console.log("book added to best books", bestBook);
              books
                .findOne({ id: book.id })
                .then(
                  book => {
                    helpers.doneAndSendPayload(res, bestBook);
                    if (book == null) {
                      books.create(req.body).then(book => {
                        console.log("book posted in Books", book);
                      });
                    } else {
                      console.log("book already in Books collection", book);
                    }
                  },
                  err => next(err)
                )
                .catch(err => next(err));
            });
          } else {
            helpers.bookAlreadyInDatabase(bestBook, next);
            // If book is already present we do not make actions here but in PUT for modifying the fields
          }
        },
        err => next(err)
      )
      .catch(err => next(err));
  })
  .put(authenticate.verifyUser, (req, res, next) => {
    let book = req.body;
    bestBooks
      .findOne({ googleId: book.id }) // find in your best books (our userId)
      .then(
        bestBook => {
          if (bestBook != null) {
            if (bestBook.userOwners.indexOf(req.params.userId) > -1) {
              helpers.UserAlreadyInOwners(bestBook, req.params.userId, next);
            } else {
              bestBook
                .update({ $push: { userOwners: req.params.userId } })
                .then(
                  book => {
                    console.log(
                      "book updated in user " +
                        req.params.userId +
                        " collection",
                      book
                    );
                    helpers.doneAndSendPayload(res, book);
                  },
                  err => next(err)
                )
                .catch(err => next(err));
            }
          } else {
            helpers.bookNotFound(bestBook, next);
          }
        },
        err => next(err)
      )
      .catch(err => next(err));
  })
  .delete(authenticate.verifyUser, (req, res, next) => {
    let book = req.body;
    // if book contains id delete specific book
    if (book.id) {
      bestBooks
        .findOne({ googleId: book.id }) // find in your best books (our userId)
        .then(
          bestBook => {
            if (bestBook.userOwners.indexOf(req.params.userId) > -1) {
              bestBook
                .update({ $pull: { userOwners: req.params.userId } })
                .then(
                  bestBook => {
                    let feed = `Book ${book.id} deleted from user: ${
                      req.params.userId
                    } in best books`;
                    console.log(feed);
                    helpers.doneAndSendPayload(res, bestBook);
                  },
                  err => next(err)
                )
                .catch(err => next(err));
            } else {
              helpers.bookOrUserNotFound(bestBook, req.params.userId, next);
            }
          },
          err => next(err)
        )
        .catch(err => next(err));
    } else {
      // delete user form all books
      bestBooks
        .find({})
        .then(docs => {
          docs.forEach(document => {
            let userPosition = document.userOwners.indexOf(req.params.userId);
            document.userOwners.splice(userPosition, 1);
            document.save();
          });
        })
        .then(
          docs => {
            helpers.doneAndSendPayload(
              res,
              "all best books deleted from the list"
            );
          },
          err => next(err)
        )
        .catch(err => next(err));
    }
  });

module.exports = bestBooksRouter;
