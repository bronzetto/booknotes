const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
mongoose.set("debug", true);
const helpers = require("../helperFunc");
const booksRouter = express.Router();

const books = require("../models/books");
const bestBooks = require("../models/bestBooks");
const wishBooks = require("../models/wishBooks");
const dislikedBooks = require("../models/dislikedBooks");

const authenticate = require("../authenticate");

booksRouter.use(bodyParser.json());

booksRouter
  .route("/")
  .get(authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    books
      .find({})
      .then(books => helpers.doneAndSendPayload(res, books), err => next(err))
      .catch(err => next(err));
  })
  .post(authenticate.verifyUser, (req, res, next) => {
    helpers.notSupported(req, res);
  })
  .put(authenticate.verifyUser, (req, res, next) => {
    helpers.notSupported(req, res);
  })
  .delete(authenticate.verifyUser, (req, res, next) => {
    helpers.notSupported(req, res);
  });

booksRouter
  .route("/:userId")
  .get(authenticate.verifyUser, (req, res, next) => {
    bestBooks
      .find({ userOwners: { $all: [req.params.userId] } })
      .then(
        books => {
          return books;
        },
        err => next(err)
      )
      .then(result => {
        wishBooks
          .find({ userOwners: { $all: [req.params.userId] } })
          .then(
            books => {
              return result.concat(books);
            },
            err => next(err)
          )
          .then(result => {
            dislikedBooks
              .find({ userOwners: { $all: [req.params.userId] } })
              .then(
                books => {
                  return result.concat(books);
                },
                err => next(err)
              )
              .then(result => {
                helpers.doneAndSendPayload(res, result);
              })
              .catch(err => next(err));
          });
      });
  })
  .post(authenticate.verifyUser, (req, res, next) => {
    let book = req.body;
    books
      .findOne({ id: book.id })
      .then(
        book => {
          if (book == null) {
            books.create(req.body).then(book => {
              console.log("book posted in Books", book);
            });
          } else {
            console.log("book already in Books collection", book);
          }
        },
        err => next(err)
      )
      .catch(err => next(err));
  })
  .put(authenticate.verifyUser, (req, res, next) => {
    helpers.notSupported(req, res);
  })
  .delete(authenticate.verifyUser, (req, res, next) => {
    bestBooks
      .find({ userOwners: { $all: [req.params.userId] } })
      .then(
        books => {
          books.forEach(book => {
            let userPosition = book.userOwners.indexOf(req.params.userId);
            book.userOwners.splice(userPosition, 1);
            book.save();
          });
          return books;
        },
        err => next(err)
      )
      .then(result => {
        wishBooks
          .find({ userOwners: { $all: [req.params.userId] } })
          .then(
            books => {
              books.forEach(book => {
                let userPosition = book.userOwners.indexOf(req.params.userId);
                book.userOwners.splice(userPosition, 1);
                book.save();
              });
              return books;
            },
            err => next(err)
          )
          .then(result => {
            dislikedBooks
              .find({ userOwners: { $all: [req.params.userId] } })
              .then(
                books => {
                  books.forEach(book => {
                    let userPosition = book.userOwners.indexOf(
                      req.params.userId
                    );
                    book.userOwners.splice(userPosition, 1);
                    book.save();
                  });
                  return books;
                },
                err => next(err)
              )
              .then(result => {
                console.log("User books deleted");

                helpers.doneAndSendPayload(res, []);
              })
              .catch(err => next(err));
          });
      });
  });

module.exports = booksRouter;
