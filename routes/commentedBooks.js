const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const helpers = require("../helperFunc");

const comments = require("../models/comments");
const books = require("../models/books");

const commentedBooksRouter = express.Router();

const authenticate = require("../authenticate");

commentedBooksRouter.use(bodyParser.json());

commentedBooksRouter
  .route("/")
  .get(authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    let response = [];
    comments
      .find({})
      .populate("bookRef", "volumeInfo")
      .then(
        comments => {
          comments.forEach(comment => {
            let shortBook = {};
            (shortBook.googleId = comment.bookRef.id),
              (shortBook.title = comment.bookRef.volumeInfo.title),
              (shortBook.description = comment.bookRef.volumeInfo.description),
              (shortBook.comment = comment.comment),
              (shortBook.myRating = comment.myRating),
              (shortBook.categories = comment.bookRef.volumeInfo.categories),
              (shortBook.image =
                comment.bookRef.volumeInfo.imageLinks.thumbnail),
              (shortBook.averageRating =
                comment.bookRef.volumeInfo.averageRating);

            response.push(shortBook);
          });
          helpers.doneAndSendPayload(res, response);
        },
        err => next(err)
      )
      .catch(err => next(err));
  })
  .post(authenticate.verifyUser, (req, res, next) => {
    helpers.notSupported(req, res);
  })
  .put(authenticate.verifyUser, (req, res, next) => {
    helpers.notSupported(req, res);
  })
  .delete(authenticate.verifyUser, (req, res, next) => {
    helpers.notSupported(req, res);
  });

commentedBooksRouter
  .route("/:userId")
  .get(authenticate.verifyUser, (req, res, next) => {
    let response = [];
    let arrBooksId = [];

    comments
      .find({})
      .populate("bookRef", "volumeInfo")
      .then(
        comments => {
          comments.forEach(comment => {
            let shortBook = {};
            if (comment.author == req.params.userId) {
              (shortBook.googleId = comment.bookRef.id),
                (shortBook.title = comment.bookRef.volumeInfo.title),
                (shortBook.description =
                  comment.bookRef.volumeInfo.description),
                (shortBook.comment = comment.comment),
                (shortBook.myRating = comment.myRating),
                (shortBook.categories = comment.bookRef.volumeInfo.categories),
                (shortBook.image = comment.bookRef.volumeInfo.imageLinks),
                (shortBook.averageRating =
                  comment.bookRef.volumeInfo.averageRating);

              response.push(shortBook);
            }
          });
          helpers.doneAndSendPayload(res, response);
        },
        err => next(err)
      )
      .catch(err => next(err));
  })
  .post(authenticate.verifyUser, (req, res, next) => {
    let comment = req.body;
    comment.author = req.params.userId;
    comments
      .create(comment)
      .then(
        comment => {
          helpers.doneAndSendPayload(res, comment);
        },
        err => next(err)
      )
      .catch(err => next(err));
  })
  .put(authenticate.verifyUser, (req, res, next) => {
    comments
      .findOneAndUpdate(
        req.body._id,
        {
          $set: { myRating: req.body.myRating, comment: req.body.comment }
        },
        { new: true }
      )
      .then(
        comment => {
          helpers.doneAndSendPayload(res, comment);
        },
        err => next(err)
      )
      .catch(err => next(err));
  })
  .delete(authenticate.verifyUser, (req, res, next) => {
    if (req.body.author == req.params.userId) {
      comments
        .findOneAndDelete(req.body._id)
        .then(
          comment => {
            helpers.doneAndSendPayload(res, comment);
          },
          err => next(err)
        )
        .catch(err => next(err));
    } else {
      helpers.UserNotFound(req.params.userId, next);
    }
  });

module.exports = commentedBooksRouter;
