const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const helpers = require("../helperFunc");

const commentsRouter = express.Router();

commentsRouter
  .route("/")
  .get((req, res, next) => {
    // comments
    // .find({})
    // .then(comments => {

    // })
    helpers.notSupported(req, res);
  })
  .post((req, res, next) => {
    helpers.notSupported(req, res);
  })
  .put((req, res, next) => {
    helpers.notSupported(req, res);
  })
  .delete((req, res, next) => {
    helpers.notSupported(req, res);
  });

commentsRouter
  .route("/:user")
  .get((req, res, next) => {
    helpers.ok(req, res);
  })
  .post((req, res, next) => {
    helpers.notSupported(req, res);
  })
  .put((req, res, next) => {
    helpers.notSupported(req, res);
  })
  .delete((req, res, next) => {
    helpers.ok(req, res);
  });

commentsRouter
  .route("/:user/:bookId")
  .get((req, res, next) => {
    helpers.ok(req, res);
  })
  .post((req, res, next) => {
    helpers.notSupported(req, res);
  })
  .put((req, res, next) => {
    helpers.ok(req, res);
  })
  .delete((req, res, next) => {
    helpers.ok(req, res);
  });

commentsRouter
  .route("/:user/:bookId/comments")
  .get((req, res, next) => {
    helpers.ok(req, res);
  })
  .post((req, res, next) => {
    helpers.ok(req, res);
  })
  .put((req, res, next) => {
    helpers.notSupported(req, res);
  })
  .delete((req, res, next) => {
    helpers.ok(req, res);
  });

commentsRouter
  .route("/:user/:bookId/comments/:commentId")
  .get((req, res, next) => {
    helpers.ok(req, res);
  })
  .post((req, res, next) => {
    helpers.notSupported(req, res);
  })
  .put((req, res, next) => {
    helpers.ok(req, res);
  })
  .delete((req, res, next) => {
    helpers.ok(req, res);
  });

module.exports = commentsRouter;
