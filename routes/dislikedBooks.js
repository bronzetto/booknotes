const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const helpers = require("../helperFunc");

const dislikedBooks = require("../models/dislikedBooks");

const dislikedBooksRouter = express.Router();

const books = require("../models/books");

const authenticate = require("../authenticate");

const cors = require("./cors");

dislikedBooksRouter.use(bodyParser.json());

dislikedBooksRouter
  .route("/")
  .get(authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    dislikedBooks
      .find({})
      .then(
        dislikedBooks => helpers.doneAndSendPayload(res, dislikedBooks),
        err => next(err)
      )
      .catch(err => next(err));
  })
  .post((req, res, next) => {
    helpers.notSupported(req, res);
  })
  .put((req, res, next) => {
    helpers.notSupported(req, res);
  })
  .delete((req, res, next) => {
    helpers.notSupported(req, res);
  });

dislikedBooksRouter
  .route("/:userId")
  .options(cors.corsWithOptions, (req, res) => {
    res.sendStatus(200);
  })
  .get(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    let book = req.body;
    dislikedBooks
      .find({})
      .then(
        dislikedBook => {
          console.log(dislikedBook, req.params.userId, "!!!!!!!!!!!");
          let response = [];
          dislikedBook.forEach(el => {
            if (el.userOwners.indexOf(req.params.userId) > -1) {
              response.push(el);
            }
          });
          return response;
        },
        err => next(err)
      )
      .then(response => {
        helpers.doneAndSendPayload(res, response);
      })
      .catch(err => next(err));
  })
  .post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    let book = req.body;
    console.log("I AM ISNIDE!!!!!!!!");

    dislikedBooks
      .findOne({ googleId: book.id }) // find in your disliked books (our userId)
      .then(
        dislikedBook => {
          if (dislikedBook == null) {
            let thisImage = book.image || "";
            let newdislikedBook = {
              googleId: book.googleId,
              title: book.title,
              authors: book.authors,
              categories: book.categories,
              image: thisImage,
              averageRating: book.averageRating,
              userOwners: req.params.userId
            };
            dislikedBooks
              .create(newdislikedBook)
              .then(dislikedBook => {
                console.log("book added to disliked books", dislikedBook);
                books
                  .findOne({ id: book.id })
                  .then(book => {
                    helpers.doneAndSendPayload(res, dislikedBook);

                    if (book == null) {
                      books.create(req.body).then(book => {
                        console.log("book posted in Books", book);
                      });
                    } else {
                      console.log("book already in Books collection", book);
                    }
                  })
                  .catch(err => next(err));
              })
              .catch(err => next(err));
          } else {
            helpers.bookAlreadyInDatabase(dislikedBook, next);
            // If book is already present we do not make actions here but in PUT for modifying the fields
          }
        },
        err => next(err)
      )
      .catch(err => next(err));
  })
  .put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    let book = req.body;
    dislikedBooks
      .findOne({ googleId: book.id }) // find in your disliked books (our userId)
      .then(
        dislikedBook => {
          if (dislikedBook != null) {
            if (dislikedBook.userOwners.indexOf(req.params.userId) > -1) {
              helpers.UserAlreadyInOwners(
                dislikedBook,
                req.params.userId,
                next
              );
            } else {
              dislikedBook
                .update({ $push: { userOwners: req.params.userId } })
                .then(
                  book => {
                    console.log(
                      "book updated in user " +
                        req.params.userId +
                        " collection",
                      book
                    );
                    helpers.doneAndSendPayload(res, book);
                  },
                  err => next(err)
                )
                .catch(err => next(err));
            }
          } else {
            // helpers.bookNotFound(dislikedBook, next);
            res.statusCode = 204;
            res.json([]);
          }
        },
        err => next(err)
      )
      .catch(err => next(err));
  })
  .delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    let book = req.body;
    // if book contains id delete specific book
    if (book.id) {
      dislikedBooks
        .findOne({ googleId: book.id }) // find in your disliked books (our userId)
        .then(
          dislikedBook => {
            if (dislikedBook.userOwners.indexOf(req.params.userId) > -1) {
              dislikedBook
                .update({ $pull: { userOwners: req.params.userId } })
                .then(
                  dislikedBook => {
                    let feed = `Book ${book.id} deleted from user: ${
                      req.params.userId
                    } in disliked books`;
                    console.log(feed);
                    helpers.doneAndSendPayload(res, dislikedBook);
                  },
                  err => next(err)
                )
                .catch(err => next(err));
            } else {
              helpers.bookOrUserNotFound(dislikedBook, req.params.userId, next);
            }
          },
          err => next(err)
        )
        .catch(err => next(err));
    } else {
      // delete user form all books
      dislikedBooks
        .find({})
        .then(docs => {
          docs.forEach(document => {
            let userPosition = document.userOwners.indexOf(req.params.userId);
            document.userOwners.splice(userPosition, 1);
            document.save();
          });
        })
        .then(
          docs => {
            helpers.doneAndSendPayload(
              res,
              "all disliked books deleted from the list"
            );
          },
          err => next(err)
        )
        .catch(err => next(err));
    }
  });

module.exports = dislikedBooksRouter;
