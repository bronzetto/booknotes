const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
mongoose.set("debug", true);
const helpers = require("../helperFunc");
const myBooksRouter = express.Router();

const myBooks = require("../models/myBooks");
const books = require("../models/books");

const authenticate = require("../authenticate");

const cors = require("./cors");

myBooksRouter.use(bodyParser.json());

myBooksRouter
  .route("/")
  .get(authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    myBooks
      .find({})
      .then(
        myBooks => helpers.doneAndSendPayload(res, myBooks),
        err => next(err)
      )
      .catch(err => next(err));
  })
  .post(authenticate.verifyUser, (req, res, next) => {
    helpers.notSupported(req, res);
  })
  .put(authenticate.verifyUser, (req, res, next) => {
    helpers.notSupported(req, res);
  })
  .delete(authenticate.verifyUser, (req, res, next) => {
    helpers.notSupported(req, res);
  });

myBooksRouter
  .route("/:userId")
  .options(cors.corsWithOptions, (req, res) => {
    res.sendStatus(200);
  })
  .get(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    let book = req.body;
    myBooks
      .find({})
      .then(
        myBook => {
          let response = [];
          myBook.forEach(el => {
            if (el.userOwners.indexOf(req.params.userId) > -1) {
              response.push(el);
            }
          });
          return response;
        },
        err => next(err)
      )
      .then(response => {
        helpers.doneAndSendPayload(res, response);
      })
      .catch(err => next(err));
  })
  .post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    let book = req.body;
    console.log(
      "==================================",
      book,
      "=================================="
    );

    myBooks
      .findOne({ googleId: book.id }) // find in your best books (our userId)
      .then(
        myBook => {
          if (myBook == null) {
            let thisImage = book.image || "";
            let newBook = {
              googleId: book.googleId,
              title: book.title,
              authors: book.authors,
              categories: book.categories,
              image: thisImage,
              averageRating: book.averageRating,
              userOwners: req.params.userId
            };
            myBooks.create(newBook).then(myBook => {
              console.log("book added to my books", myBook);
              books
                .findOne({ id: book.id })
                .then(
                  book => {
                    helpers.doneAndSendPayload(res, myBook);
                    if (book == null) {
                      books.create(req.body).then(book => {
                        console.log("book posted in Books", book);
                      });
                    } else {
                      console.log("book already in Books collection", book);
                    }
                  },
                  err => next(err)
                )
                .catch(err => next(err));
            });
          } else {
            helpers.bookAlreadyInDatabase(myBook, next);
            // If book is already present we do not make actions here but in PUT for modifying the fields
          }
        },
        err => next(err)
      )
      .catch(err => next(err));
  })
  .put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    let book = req.body;
    myBooks
      .findOne({ googleId: book.id }) // find in your best books (our userId)
      .then(
        myBook => {
          if (myBook != null) {
            if (myBook.userOwners.indexOf(req.params.userId) > -1) {
              helpers.UserAlreadyInOwners(myBook, req.params.userId, next);
            } else {
              myBook
                .update({ $push: { userOwners: req.params.userId } })
                .then(
                  book => {
                    console.log(
                      "book updated in user " +
                        req.params.userId +
                        " collection",
                      book
                    );
                    helpers.doneAndSendPayload(res, book);
                  },
                  err => next(err)
                )
                .catch(err => next(err));
            }
          } else {
            // helpers.bookNotFound(myBook, next);
            res.statusCode = 204;
            res.json([]);
          }
        },
        err => next(err)
      )
      .catch(err => next(err));
  })
  .delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    let book = req.body;
    // if book contains id delete specific book
    if (book.id) {
      myBooks
        .findOne({ googleId: book.id }) // find in your best books (our userId)
        .then(
          myBook => {
            if (myBook.userOwners.indexOf(req.params.userId) > -1) {
              myBook
                .update({ $pull: { userOwners: req.params.userId } })
                .then(
                  myBook => {
                    let feed = `Book ${book.id} deleted from user: ${
                      req.params.userId
                    } in my books`;
                    console.log(feed);
                    helpers.doneAndSendPayload(res, myBook);
                  },
                  err => next(err)
                )
                .catch(err => next(err));
            } else {
              helpers.bookOrUserNotFound(myBook, req.params.userId, next);
            }
          },
          err => next(err)
        )
        .catch(err => next(err));
    } else {
      // delete user form all books
      myBooks
        .find({})
        .then(docs => {
          docs.forEach(document => {
            let userPosition = document.userOwners.indexOf(req.params.userId);
            document.userOwners.splice(userPosition, 1);
            document.save();
          });
        })
        .then(
          docs => {
            helpers.doneAndSendPayload(
              res,
              "all my books deleted from the list"
            );
          },
          err => next(err)
        )
        .catch(err => next(err));
    }
  });

module.exports = myBooksRouter;
