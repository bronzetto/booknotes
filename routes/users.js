const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");

const cors = require("./cors");

const User = require("../models/users");
const authenticate = require("../authenticate");

router.use(bodyParser.json());
const passport = require("passport");
const mongoose = require("mongoose");
const helpers = require("../helperFunc");

let jwt = require("jsonwebtoken");

const main = require("../modules/sendEmails/sendEmails");

router.get(
  "/",
  authenticate.verifyUser,
  authenticate.verifyAdmin,
  (req, res, next) => {
    User.find({})
      .then(
        users => {
          res.statusCode = 200;
          res.setHeader("Content-Type", "application/json");
          res.json(users);
        },
        err => next(err)
      )
      .catch(err => next(err));
  }
);

router
  .route("/signup")
  .options(cors.corsWithOptions, (req, res) => {
    res.sendStatus(200);
  })
  .post(cors.corsWithOptions, (req, res, next) => {
    // generate jwt and send email for email verification
    let token = jwt.sign(
      {
        user: req.body
      },
      "email verification arroduco",
      { expiresIn: "1h" }
    );

    let urlForEmailConfirmation = `${req.protocol}://${req.get(
      "host"
    )}/users/verify-user/${token}`;

    console.log("==>", urlForEmailConfirmation, req.body, token);
    main(
      {
        type: "email-verification",
        verificationUrl: urlForEmailConfirmation,
        userDestination: req.body.username
      },
      res
    ).catch(error => next(error));
  });

router
  .route("/verify-user/:token")
  .options(cors.corsWithOptions, (req, res) => {
    res.sendStatus(200);
  })
  .get(cors.corsWithOptions, (req, res) => {
    let baseUrlApp = `${req.protocol}://${req.get("host")}`;
    const token = req.params.token;
    jwt.verify(token, "email verification arroduco", (err, decoded) => {
      if (err) {
        res.redirect(baseUrlApp + "?registeredUnsuccessfull=" + err.name);
      } else {
        const user = decoded.user;
        if (!err) {
          User.register(
            new User({
              username: user.username,
              firstName: user.firstName,
              lastName: user.lastName
            }),
            user.password,
            (err, user) => {
              let baseUrlApp = `${req.protocol}://${req.get("host")}`;
              if (err) {
                res.redirect(
                  baseUrlApp + "?registeredUnsuccessfull=" + err.name
                );
              } else if (user) {
                res.redirect(baseUrlApp + "?registeredSuccessfull");
              }
            }
          );
        }
      }
    });
  });

router
  .route("/login")
  .options(cors.corsWithOptions, (req, res) => {
    res.sendStatus(200);
  })
  .post(cors.corsWithOptions, (req, res, next) => {
    passport.authenticate("local", (err, user, info) => {
      if (err) next(err);

      if (!user) helpers.logInFailed(res, info);

      req.logIn(user, err => {
        if (err) helpers.logInFailed("Could not login!");
        let token = authenticate.getToken({ _id: req.user._id });
        let userLogged = {
          firstName: user.firstName,
          lastName: user.lastName,
          id: user._id,
          token: token,
          email: user.username
        };
        helpers.logInSuccess(res, userLogged, token);
      });
    })(req, res, next);
  });

router
  .route("/request-reset-password")
  .options(cors.corsWithOptions, (req, res) => {
    res.sendStatus(200);
  })
  .post(cors.corsWithOptions, (req, res, next) => {
    let { email } = req.body.email;

    User.find({ username: email })
      .then(
        user => {
          if (user) {
            // generate jwt and send email for email verification
            let token = jwt.sign(
              {
                user: req.body
              },
              "password reset chegua",
              { expiresIn: "1h" }
            );
            let urlForPasswordChange = `${req.protocol}://${req.get(
              "host"
            )}/users/reset-password/${token}`;

            main(
              {
                type: "password-change",
                verificationUrl: urlForPasswordChange,
                userDestination: email
              },
              res
            ).catch(error => next(error));
          } else {
            helpers.UserNotFound(user.username, next);
          }
        },
        err => next(err)
      )
      .catch(err => next(err));
  });

router
  .route("/reset-password/:token")
  .options(cors.corsWithOptions, (req, res) => {
    res.sendStatus(200);
  })
  .get(cors.corsWithOptions, (req, res, next) => {
    let token = req.params.token;
    let baseUrlApp = `${req.protocol}://${req.get("host")}`;
    console.log("==>???????", token);
    jwt.verify(token, "password reset chegua", (err, decoded) => {
      const user = decoded.user;
      console.log("");

      if (err) {
        console.log("error from GET!!!!!!!!!!");

        res.redirect(baseUrlApp + "?passwordChangeUnsuccessfull=" + err.name);
      } else {
        console.log("REQUEST CHANGE PASSWORD________________");
        // generate another token and send it to app
        let token = jwt.sign(
          {
            email: user.email.email
          },
          "password reset second stage arrori",
          { expiresIn: "1h" }
        );
        let changeOldPassUrl = `${baseUrlApp}?replace-password/${token}`;
        res.redirect(changeOldPassUrl);
      }
    });
  })
  .post(cors.corsWithOptions, (req, res, next) => {
    let token = req.params.token;
    let baseUrlApp = `${req.protocol}://${req.get("host")}`;
    console.log("==>", token);
    jwt.verify(token, "password reset second stage arrori", (err, decoded) => {
      console.log("--------", err, "--------", decoded);

      if (err) {
        console.log("error!!!!!!!!!!", err.name);

        // res.redirect(baseUrlApp + "?passwordChangeUnsuccessfull=" + err.name);
        helpers.jwtInvalid(res, err);
      } else {
        console.log("_____________LLLLL", decoded);
        const userEmail = decoded;

        User.find({ username: decoded.email })
          .then(
            user => {
              user = user[0];
              if (user) {
                let pass = req.body.newPassword;
                // let oldPassword = req.body.oldPassword;
                // let newPassword = req.body.newPassword;
                console.log("USERRRRRR!!!!!!", pass, user);

                user
                  .setPassword(pass)

                  // .changePassword("123455", "123457")
                  .then(
                    result => {
                      user.save();
                      let us = {
                        firstName: result.firstName,
                        lastName: result.lastName,
                        username: result.username
                      };
                      console.log("changed!!", result);
                      helpers.doneAndSendPayload(res, us);
                    },
                    err => next(err)
                  )
                  .catch(err => next(err));
              } else {
                console.log("NOTTTTUSERRRRRR!!!!!!");
                helpers.UserNotFound(user, next);
              }
            },
            err => next(err)
          )
          .catch(err => next(err));
      }
    });
  });

router.get("/logout", (req, res, next) => {
  console.log(req);

  req.logout();
  // if (req.session) {
  //   req.session.destroy();
  //   res.clearCookie("session-id");
  //   res.redirect("/");
  // } else {
  //   var err = new Error("You are not logged in!");
  //   err.status = 403;
  //   next(err);
  // }
});

router
  .route("/checkJwt")
  .options(cors.corsWithOptions, (req, res) => {
    res.sendStatus(200);
  })
  .get(cors.corsWithOptions, (req, res) => {
    passport.authenticate("jwt", { session: false }, (err, user, info) => {
      if (err) next(err);

      if (!user) {
        helpers.jwtInvalid(res, info);
      } else {
        helpers.jwtValid(res, user);
      }
    })(req, res);
  });

module.exports = router;
