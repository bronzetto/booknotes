const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const helpers = require("../helperFunc");

const wishBooks = require("../models/wishBooks");

const wishBooksRouter = express.Router();

const books = require("../models/books");

const authenticate = require("../authenticate");

const cors = require("./cors");

wishBooksRouter.use(bodyParser.json());

wishBooksRouter
  .route("/")
  .get(authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    wishBooks
      .find({})
      .then(
        wishBooks => helpers.doneAndSendPayload(res, wishBooks),
        err => next(err)
      )
      .catch(err => next(err));
  })
  .post((req, res, next) => {
    helpers.notSupported(req, res);
  })
  .put((req, res, next) => {
    helpers.notSupported(req, res);
  })
  .delete((req, res, next) => {
    helpers.notSupported(req, res);
  });

wishBooksRouter
  .route("/:userId")
  .options(cors.corsWithOptions, (req, res) => {
    res.sendStatus(200);
  })
  .get(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    let book = req.body;
    wishBooks
      .find({})
      .then(
        wishBook => {
          console.log(wishBook, req.params.userId, "!!!!!!!!!!!");
          let response = [];
          wishBook.forEach(el => {
            if (el.userOwners.indexOf(req.params.userId) > -1) {
              response.push(el);
            }
          });
          return response;
        },
        err => next(err)
      )
      .then(response => {
        helpers.doneAndSendPayload(res, response);
      })
      .catch(err => next(err));
  })
  .post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    let book = req.body;
    wishBooks
      .findOne({ googleId: book.id }) // find in your wish books (our userId)
      .then(
        wishBook => {
          if (wishBook == null) {
            let thisImage = book.image || "";
            let newWishBook = {
              googleId: book.googleId,
              title: book.title,
              authors: book.authors,
              categories: book.categories,
              image: thisImage,
              averageRating: book.averageRating,
              userOwners: req.params.userId
            };
            wishBooks
              .create(newWishBook)
              .then(wishBook => {
                console.log("book added to wish books", wishBook);
                books
                  .findOne({ id: book.id })
                  .then(book => {
                    helpers.doneAndSendPayload(res, wishBook);
                    if (book == null) {
                      books.create(req.body).then(book => {
                        console.log("book posted in Books", book);
                      });
                    } else {
                      console.log("book already in Books collection", book);
                    }
                  })
                  .catch(err => next(err));
              })
              .catch(err => next(err));
          } else {
            helpers.bookAlreadyInDatabase(wishBook, next);
            // If book is already present we do not make actions here but in PUT for modifying the fields
          }
        },
        err => next(err)
      )
      .catch(err => next(err));
  })
  .put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    let book = req.body;
    wishBooks
      .findOne({ googleId: book.id }) // find in your wish books (our userId)
      .then(
        wishBook => {
          if (wishBook != null) {
            if (wishBook.userOwners.indexOf(req.params.userId) > -1) {
              helpers.UserAlreadyInOwners(wishBook, req.params.userId, next);
            } else {
              wishBook
                .update({ $push: { userOwners: req.params.userId } })
                .then(
                  book => {
                    console.log(
                      "book updated in user " +
                        req.params.userId +
                        " collection",
                      book
                    );
                    helpers.doneAndSendPayload(res, book);
                  },
                  err => next(err)
                )
                .catch(err => next(err));
            }
          } else {
            // helpers.bookNotFound(wishBook, next, res);
            res.statusCode = 204;
            res.json([]);
          }
        },
        err => next(err)
      )
      .catch(err => next(err));
  })
  .delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    let book = req.body;
    // if book contains id delete specific book
    if (book.id) {
      wishBooks
        .findOne({ googleId: book.id }) // find in your wish books (our userId)
        .then(
          wishBook => {
            if (wishBook.userOwners.indexOf(req.params.userId) > -1) {
              wishBook
                .update({ $pull: { userOwners: req.params.userId } })
                .then(
                  wishBook => {
                    let feed = `Book ${book.id} deleted from user: ${
                      req.params.userId
                    } in wish books`;
                    console.log(feed);
                    helpers.doneAndSendPayload(res, wishBook);
                  },
                  err => next(err)
                )
                .catch(err => next(err));
            } else {
              helpers.bookOrUserNotFound(wishBook, req.params.userId, next);
            }
          },
          err => next(err)
        )
        .catch(err => next(err));
    } else {
      // delete user form all books
      wishBooks
        .find({})
        .then(docs => {
          docs.forEach(document => {
            let userPosition = document.userOwners.indexOf(req.params.userId);
            document.userOwners.splice(userPosition, 1);
            document.save();
          });
        })
        .then(
          docs => {
            helpers.doneAndSendPayload(
              res,
              "all wish books deleted from the list"
            );
          },
          err => next(err)
        )
        .catch(err => next(err));
    }
  });

module.exports = wishBooksRouter;
